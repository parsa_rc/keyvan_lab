<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
//            $table->integer('round_id');
            $table->string('name');
            $table->string('parent');
            $table->string('phone')->unique();
            $table->string('melli_code',10)->unique();
            $table->integer('age');
//            $table->string('password');
            $table->boolean('isPayed')->default(false);
            $table->longText('address');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

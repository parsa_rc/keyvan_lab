@extends('layouts.admin.template')

@section('title','تعیین هزینه ثبت نام')

@section('content')
    <div class="col-md-4 col-sm-7 col-12 mx-auto iransans-web">
        <form class="form" method="post" action="{{ route('price.change') }}">
            {{ csrf_field() }}
            <div class="form-group text-right">
                <label for="price" dir="rtl">قیمت:<span class="font-s">(تومان)</span></label>
                <input type="text" id="price" name="price" class="form-control" value="{{ $price ?: null }}" autocomplete="off" placeholder="هزینه آزمایش را بر حسب تومان وارد کنید" dir="rtl">
            </div>
            <div class="form-group text-left">
                <input type="submit" class="btn btn-primary" value="ثبت">
            </div>
        </form>
    </div>
@endsection
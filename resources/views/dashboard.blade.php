@extends('layouts.app')

@section('title',$user->name.'پنل کاربر-')

@section('content')
    @include('layouts.header')
    <hr class="m-0 p-0">
    <div class="col-md-8 col-12 mx-auto text-right my-5 iransans-web">
        <h3 class="mr-1">{{ $user->name }}</h3>
        @php $attachments = $user->attachments @endphp
        @if(count($attachments))
            <ul class="list-group list-group-flush">
                <li class="list-group-item list-group-item-secondary">
                    نتیجه آزمایشات
                </li>
                @foreach($attachments as $attachment)
                    @php $round = $attachment->round @endphp
                    <li class="list-group-item">
                        <a href="{{ route('attachment.download' , ['round_id' => $round->id , 'user_id' => $attachment->user->id])  }}" class="text-secondary">
                            <span>{{ 'نتیجه آزمایش روز '.toFaDigits(\Morilog\Jalali\Jalalian::forge($round->date)->format('%A, %d %B %y').' ساعت '.$round->end.' - '.$round->start) }}</span>
                            <span class="icon icon-file mx-2 font-xl"></span>
                        </a>
                    </li>
                @endforeach
            </ul>
        @else
            <div class="alert alert-info" dir="rtl">
                نتیجه آزمایشی برای شما ثبت نشده است.
            </div>
        @endif
    </div>
@endsection
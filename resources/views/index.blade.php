@extends('layouts.app')

@section('title','صفحه اصلی')

@section('style')
    <link type="text/css" rel="stylesheet" href="{{ asset('style/slick.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('style/slick-theme.css') }}">
    <style>
        .box {
            border-radius: 1px;
            box-shadow: 0 0 2px #bababa;
            margin: .4rem .25rem;
            font-family: iransans_web;
            color: #252525;
            direction: rtl;
        }

        .box-header {
            background-color: #fefefe;
            padding: .5rem;
            text-align: right;
            font-size: 14px;
        }

        .box-header > .box-title {
            font-weight: 600;
        }

        .box-date {
            font-family: iransans_web_ultralight;
            margin-bottom: 0;
            text-align: right;
        }

        .box-content {
            background-color: #eee;
            padding: .25rem;
            text-align: center;
            font-size: 15px;
        }
    </style>
@endsection

@section('content')

    <div class="page-wrapper">
        <!-- Preloader -->
        <div class="preloader"></div>

        <!-- header section -->
    @include('layouts.header')
    <!-- End header section -->

        <!-- Bnner Section -->
        <section class="banner-section">
            <div class="banner-carousel owl-carousel owl-theme">

                <!-- Slide Item -->
                <div class="slide-item" style="background-image: url(/images/main-slider/1.jpg);">
                    <div class="container xs-banner-container-parent">
                        <div class="clearfix">

                            <!-- Content Column -->
                            <div class="banner-column col-lg-6 col-md-6 col-sm-12">
                                <div class="title wow fadeInUp" data-wow-delay="250ms">معرفی</div>
                                <h2 class="wow fadeInUp" data-wow-delay="500ms">پروفسور حسین کیوانی</h2>
                                <p class="text wow fadeInUp" data-wow-delay="750ms">
                                    متخصص ویروس شناسی بالینی از کانادا
                                    عضو هیأت علمی و استاد دانشگاه علوم پزشکی ایران
                                    مؤسس آزمایشگاه تخصصی ویروس شناسی کیوان
                                    {{--پروفسور حسین کیوانی پس از اخذ دکترای عمومی در سال 1368 از دانشگاه تهران به منظور طی دوره دکترای--}}
                                    {{--تخصصی(D.ph (به فرانسه و سپس به کانادا عزیمت نمود. وی در سال 1375 پس از اخذ دکترای تخصصی (D.ph (از--}}
                                    {{--دانشگاه شربروک کانادا که، در طول تحصیل وی را »استثنایی« ارزیابی نمود؛ به کشور بازگشت.--}}
                                </p>
                                <div class="link-box wow fadeInUp" data-wow-delay="1000ms">
                                    <a href="{{ route('about') }}" class="theme-btn btn-style-two"><i>با پروفسور حسین کیوانی بیشتر آشنا شوید</i> <span class="arrow icon icon-arrow_right"></span></a>
                                </div>
                            </div>

                            <!-- Image Column -->

                            <div class="image">
{{--                                <img src="{{ asset('images/main-slider/content-image-1.png') }}" alt=""/>--}}
                            </div>


                        </div>

                    </div>
                </div>

                <!-- Slide Item -->
                <div class="slide-item" style="background-image: url(/images/main-slider/1.jpg);">
                    <div class="container xs-banner-container-parent">
                        <div class="clearfix">

                            <!-- Content Column -->
                            <div class="banner-column col-lg-6 col-md-6 col-sm-12">
                                <div class="title wow fadeInUp" data-wow-delay="250ms">توصیه‌های کاربردی</div>
                                <h2 class="wow fadeInUp" data-wow-delay="500ms">پیشگیری از ابتلا به ویروس کرونا</h2>
                                <p class="text wow fadeInUp" data-wow-delay="750ms">
                                    شست‌و‌شوی دست‌ها, رعایت فاصله با دیگران, عدم لمس چشم‌ها بینی و دهان و رعایت بهداشت تنفسی
                                </p>
                                <div class="link-box wow fadeInUp" data-wow-delay="1000ms">
                                    <a href="{{ route('health.news') }}" class="theme-btn btn-style-two"><i>همین حالا نوبت
                                            بگیرید</i> <span class="arrow icon icon-arrow_right"></span></a>
                                </div>
                            </div>

                            <!-- Image -->

                            <div class="image">
                                {{--<img src="https://lh3.googleusercontent.com/proxy/HiZ8LmLnoSgPm7rkX0MbckPnb6ize_ClfR7_1QSbhC5esRxRSAw0jYhPrHue38N5hvxsa1_rDH3bYggYefv7tPo154cGNa1RaHIICuTngP2msIEKlBz2maAf6qk4tA" alt=""/>--}}
                                <img src="{{ asset('images/main-slider/content-image-2.png') }}" alt=""/>
                            </div>


                        </div>

                    </div>
                </div>

                <!-- Slide Item -->
                <div class="slide-item" style="background-image: url(/images/main-slider/1.jpg);">
                    <div class="container xs-banner-container-parent">
                        <div class="clearfix">

                            <!-- Content Column -->
                            <div class="banner-column col-lg-6 col-md-6 col-sm-12">
                                {{--<div class="title wow fadeInUp" data-wow-delay="250ms">آزمایشگاه کیوان</div>--}}
                                <h2 class="wow fadeInUp" data-wow-delay="500ms">آزمایشگاه تخصصی ویروس شناسی کیوان</h2>
                                <p class="text wow fadeInUp" data-wow-delay="750ms">
                                    ما به همراه پزشکان تلاش می‌کنیم تا بیماری بیماران را تشخیص دهیم، برای سوالات موجود جوابی یافته، پیشرفت
                                    بیماری یا بهبودی را ارزیابی کنیم، سلامت افراد سالم را تایید نماییم و در صورت نیاز آموزش دهیم و از ایجاد عوارض
                                    جلوگیری کنیم
                                </p>
                                <div class="link-box wow fadeInUp" data-wow-delay="1000ms">
                                    <a href="{{ route('about') }}" class="theme-btn btn-style-two"><i>آزمایشگاه تخصصی ویروس شناسی کیوان</i> <span class="arrow icon icon-arrow_right"></span></a>
                                </div>
                            </div>

                            <!-- Image Column -->

                            <div class="image">
                                {{--<img src="https://insidehighered.com/sites/default/server_files/media/covid-19_logo_500px.png" alt=""/>--}}
                                <img src="{{ asset('images/main-slider/content-image-3.png') }}" alt=""/>
                            </div>


                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- End Bnner Section -->

        <!-- Services Form Section -->
        <section class="services-form-section">
            <div class="image-layer" style="background-image:url(/images/background/1.png)"></div>
            <div class="container ">

                <!-- Services Form -->
                <div class="services-form">
                    <!--Contact Form-->
                    {{--<form method="post" action="contact.html">--}}
                    <div class="row">
                        <div class="form-group button-group col-lg-4 col-md-12 col-sm-12">
                            <div class="left-curves"></div>
                            <div class="right-curves"></div>
                            <a href="#rounds-section" class="theme-btn submit-btn text-center" type="submit" name="submit-form">
                                مشاهده سانس‌ها
                            </a>
                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- End Services Form Section -->

        <!-- Featured Section -->
        <section id="featured-section" class="featured-section">
            <div class="pattern-layer" style="background-image:url(/images/background/pattern-1.png)"></div>
            <div class="container">
                <div class="row">
                    <!-- Featured Block -->
                    <div class="featured-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="image-layer" style="background-image:url(images/resource/feature-1.jpg)"></div>
                            <div class="icon-box">
                                <span class="icon flaticon-pill"></span>
                            </div>
                            <h3><a>جوابدهی آنلاین</a></h3>
                            <p dir="rtl" class="text-justify">
                                نتایج آزمایش ویروس شناسی انجام شده، پس
                                از تائید مسئول هر بخش به مسئول فنی ارائه
                                میگردد.
                                حداقل زمان لازم جهت انجام آزمایش ویروس
                                کرونا ۳ روز کاری بوده و
                                مراجعه کنندگانی که از طریق سامانه آنالین
                                پذیرش کرده‌اند می‌توانند با ورود به این
                                سامانه، جواب آزمایش خود را دریافت کنند.
                            </p>
                        </div>
                    </div>

                    <!-- Featured Block -->
                    <div class="featured-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="image-layer" style="background-image:url(/images/resource/feature-1.jpg)"></div>
                            <div class="icon-box">
                                <span class="icon icon-hourglass"></span>
                            </div>
                            <h3><a>نمونه گیری</a></h3>
                            <p dir="rtl" class="text-justify">
                                با توجه به حساسیت مرحله نمونه گیری و برای دستیابی به جواب دقیق آزمایش
                                در حداقل زمان, نمونه گیری از دو طریق نمونه گیری در محل آزمایشگاه ویروس شناسی کیوان و نمونه گیری در محل مورد نظر بیمار توسط پرسنل مجرب و کارآزموده آزمایشگاه کیوان انجام می‌شود.
                            </p>
                        </div>
                    </div>

                    <!-- Featured Block -->
                    <div class="featured-block col-lg-4 col-md-6 col-sm-12 mx-sm-0 mx-md-auto">
                        <div class="inner-box wow fadeInRight" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="image-layer" style="background-image:url(/images/resource/feature-1.jpg)"></div>
                            <div class="icon-box">
                                <span class="icon icon-bell"></span>
                            </div>
                            <h3><a>پذیرش آنلاین</a></h3>
                            <p dir="rtl" class="text-justify">
                                آزمایشگاه کیوان مفتخر
                                است که همزمان با آغاز
                                سال ۱۳۹۹ به منظور
                                جلوگیری از خطر سرایت
                                بیماری به مراجعه کنندگان
                                محترم و با هدف سهولت
                                بیشتر برای ایشان، سامانه
                                آنلاین پذیرش را مورد
                                استفاده قرار دهد.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Featured Section -->

        <!-- Fullwidth Section -->
        <section class="container-fluid">
            <div class="pattern-layer" style="background-image:url(/images/background/pattern-2.png)"></div>
            <div class="outer-section">
                <div class="clearfix">

                    <!-- Left Column -->
                    <div class="left-column">
                        <div class="inner-column">
                            <div class="shadow-one paroller" style="background-image:url(/images/icons/shadow-1.png)"
                                 data-paroller-factor="0.15" data-paroller-factor-lg="0.15"
                                 data-paroller-factor-md="0.15"
                                 data-paroller-factor-sm="0.15" data-paroller-type="foreground"
                                 data-paroller-direction="horizontal">
                            </div>
                            <div class="shadow-two paroller" style="background-image:url(/images/icons/shadow-2.png)"
                                 data-paroller-factor="-0.15" data-paroller-factor-lg="-0.15"
                                 data-paroller-factor-md="-0.15"
                                 data-paroller-factor-sm="-0.15" data-paroller-type="foreground"
                                 data-paroller-direction="vertical">
                            </div>
                            <div class="shadow-three paroller" style="background-image:url(/images/icons/shadow-3.png)"
                                 data-paroller-factor="0.15" data-paroller-factor-lg="0.15"
                                 data-paroller-factor-md="0.15"
                                 data-paroller-factor-sm="0.15" data-paroller-type="foreground"
                                 data-paroller-direction="horizontal">
                            </div>
                            <div class="image">
                                <img src="{{ asset('images/resource/image-1.png') }}" alt=""/>
                            </div>
                        </div>
                    </div>

                    <!-- Right Column -->
                    <div class="right-column">
                        <div class="inner-column">
                            <h2 dir="rtl">آزمایش ویروس کرونا (COVID-19)</h2>
                            <ul class="featured-list">
                                <li class="wow fadeInUp clearfix" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <span class="icon icon-brifecase-hospital2"></span>
                                    <div class="content">
                                        <div class="title">روش تشخیص</div>
                                        <p dir="rtl">
                                            در اکثر موارد از گلو یا گلو و بینی
                                            نمونه گرفته می‌شود. نمونه‌های
                                            گرفته شده در لابراتوارها مورد
                                            آزمایش قرار می‌گیرند. این
                                            آزمایش مبتنی بر روش موسوم به
                                            "واکنش زنجیرهای پلیمراز" انجام
                                            می‌شود. در این روش آزمایش،
                                            یک قطعه "دی ان ای" در داخل یک
                                            دستگاه ترموسایکلر کاپی و تکثیر
                                            می‌شود تا قطعات خاص "دی ان
                                            ای"، به گونه مثال از ویروس کرونا, جستجو شود. این روش آزمایش
                                            نشان می‌دهد که آیا کدام عامل
                                            بیماری و به چه تعدادی در بدن
                                            وجود دارد.
                                        </p>
                                    </div>
                                </li>
                                <li class="wow fadeInUp clearfix" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <span class="icon icon-time_watch"></span>
                                    <div class="content">
                                        <div class="title">زمان انجام تست</div>
                                        <p dir="rtl">
                                            خود آزمایش حدود پنج
                                            ساعت وقت می‌گیرد. به
                                            علاوه انتقال آن به
                                            لابراتوار نیز وقت نیاز
                                            دارد. نتیجه آن معمولاَ بعد
                                            از یک تا دو روز معلوم
                                            می‌شود.
                                            برای بیماری کرونا
                                            ویروس دو نوبت تست
                                            پی‌سی‌آر به فاصله ۲۴
                                            ساعت انجام داده می‌شود. اگر هر دو تست منفی باشد
                                            به این معنی است که فرد
                                            دچار این نوع بیماری
                                            نیست و می‌تواند ترخیص شود ولی همچنان تحت
                                            مراقبت قرار دارد.

                                        </p>
                                    </div>
                                </li>
                                <li class="wow fadeInUp clearfix" data-wow-delay="600ms" data-wow-duration="1500ms">
                                    <span class="icon icon-group"></span>
                                    <div class="content">
                                        <div class="title">چه کسانی باید تست دهند ؟</div>
                                        <p dir="rtl">
                                            بر اساس ارزیابی‌های کنونی، تنها اقامت در جاهایی که
                                            خطر ابتلا به کرونا وجود دارد، نمی‌تواند دلیلی برای
                                            اجرای آزمایش باشد.
                                            همچنین هر کسی که آبریزش و سرفه داشته باشد به
                                            معنی این نیست که به ویروس کرونا آلوده شده است.
                                            هرکسی که علائم عفونت ریه از یک عامل ویروسی
                                            نامعلوم داشته باشد و یا هر کسی که علائمی مانند سرفه،
                                            تب و مشکلات تنفسی داشته باشد و علاوه بر این با یک
                                            فرد آلوده تماس داشته یا در منطقه‌ای با خطر ابتلا به این
                                            بیماری رفت و آمد داشته است، به صورت موجه
                                            مظنون به ابتلا به این بیماری شمرده می‌شود و باید
                                            مورد آزمایش قرار بگیرد.
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section id="rounds-section" class="container-fluid bg-white pt-5">
            <div class="text-center">
                <div class="title">جهت انجام آزمایش ویروس کرونا</div>
                <h3>زمان مورد نظر خود را <span class="theme_color">انتخاب نمایید</span></h3>
            </div>
            @include('table')
        </section>

        <!--Main Footer-->
        @include('footer')
    </div>
    <!--End pagewrapper-->

    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon icon-chevron-up"></span></div>

    <!-- sidebar cart item -->
    <div class="xs-sidebar-group info-group">
        <div class="xs-overlay xs-bg-black"></div>
        <div class="xs-sidebar-widget">
            <div class="sidebar-widget-container">
                <div class="widget-heading">
                    <a href="#" class="close-side-widget">
                        <span class="icon icon-cross"></span>
                    </a>
                </div>
                <div class="sidebar-textwidget">

                    <!-- Sidebar Info Content -->
                    <div class="sidebar-info-contents">
                        <div class="content-inner">
                            <div class="logo">
                                <a href="#"><img src="/images/zlogo-2a.png" alt=""/></a>
                            </div>
                            <div class="content-box">
                                <h2>درباره ما</h2>
                                <p class="text">
                                </p>
                                <a href="#" class="theme-btn btn-style-one"><i>مشاوره</i></a>
                            </div>
                            <div class="contact-info">
                                <h2>اطلاعات تماس</h2>
                                <ul class="list-style-one">
                                    <li><span class="icon flaticon-map-1"></span>شهر ، شهرستان ، خیابان اول، کوچه دوم،
                                    </li>
                                    <li><span class="icon flaticon-telephone"></span>(000) 000-000-0000</li>
                                    <li><span class="icon flaticon-letter"></span>Medizco@gmail.com</li>
                                    <li><span class="icon flaticon-clock-2"></span>روزهای هفته: 09.00 to 18.00 جمعه:
                                        بسته
                                    </li>
                                </ul>
                            </div>
                            {{--<!-- Social Box -->--}}
                            {{--<ul class="social-box">--}}
                            {{--<li class="facebook"><a href="#" class="icon icon-facebook"></a></li>--}}
                            {{--<li class="twitter"><a href="#" class="icon icon-twitter"></a></li>--}}
                            {{--<li class="linkedin"><a href="#" class="icon icon-linkedin"></a></li>--}}
                            {{--<li class="instagram"><a href="#" class="icon icon-instagram"></a></li>--}}
                            {{--<li class="youtube"><a href="#" class="icon icon-youtube"></a></li>--}}
                            {{--</ul>--}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END sidebar widget item -->

    {{--<!-- xs modal -->--}}
    {{--<div class="zoom-anim-dialog mfp-hide modal-searchPanel" id="modal-popup-2">--}}
    {{--<div class="xs-search-panel">--}}
    {{--<form action="#" method="POST" class="xs-search-group">--}}
    {{--<input type="search" class="form-control" name="search" id="search" placeholder="جستجو">--}}
    {{--<button type="submit" class="search-button"><i class="icon icon-search"></i></button>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div><!-- End xs modal -->--}}
    <!-- end language switcher strart -->

@endsection

@section('script')
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".slide-item a").on("click", function () {
                if (this.hash !== "") {
                    console.log("t");
                    event.preventDefault();
                    var hash = this.hash;
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 1200, function () {
                        window.location.hash = hash;
                    });
                }
            });
            $('.your-class').slick({
                infinite: false,
                slidesToShow: 7,
                slidesToScroll: 1,
                arrows: true,
                responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 5,
                        dots: true
                    }
                }, {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        dots: true
                    }
                }, {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        dots: true
                    }
                }, {
                    breakpoint: 330,
                    settings: {
                        slidesToShow: 1,
                        arrows: false
                    }
                }]
            });
        });
    </script>
@endsection
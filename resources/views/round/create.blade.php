@extends('layouts.admin.template')

@section('title','ثبت سانس جدید')
<link type="text/css" rel="stylesheet" href="{{ asset('style/calendar.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('style/wickedpicker.css') }}">
<style>
    label , input , input::placeholder{
        font-family: iransans_web_light;
    }
    .form-group{
        margin-bottom: 2rem !important;
    }

    .custom-error{
        text-align: right;
        direction: rtl;
        font-weight: bold;
        font-size: 12px;
        color : #b81212;
    }
</style>
@section('style')
@endsection

@section('content')
    <div class="col-md-7 col-sm-9 col-11 mx-auto">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <a href="{{ route('round.index') }}" data-md-tooltip="بازگشت"
               class="shortcut-link shadow-sm material-icons md-tooltip md-tooltip--top">
                <img src="{{ asset('icons1/return.png') }}">
            </a>
            <h5 class="iransans-web-bold text-primary">
                ثبت سانس
            </h5>
        </div>
        <form method="post" action="{{ route('round.store') }}" class="form p-4 text-right bg-white" dir="rtl">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 col-12 form-group">
                    <label for="date">برای تاریخ :</label>
                    <input type="text" id="date" name="date" class="form-control form-control-sm" value="{{ old('date') }}" autocomplete="off" placeholder="روز ثبت سانس" dir="rtl">
                    @if($errors->has('date'))
                        <p class="custom-error">{{ $errors->first('date') }}</p>
                    @endif
                </div>
                <div class="col-md-6 col-12 form-group">
                    <label for="start">شروع از :</label>
                    <input type="text" id="start" name="start" class="form-control form-control-sm" value="{{ old('start') }}" autocomplete="off"  placeholder="ساعت دقیق شروع نوبت" dir="rtl">
                    @if($errors->has('start'))
                        <p class="custom-error">{{ $errors->first('start') }}</p>
                    @endif
                </div>
                <div class="col-md-6 col-12 form-group">
                    <label for="end">ادامه تا :</label>
                    <input type="text" id="end" name="end" class="form-control form-control-sm" value="{{ old('end') }}" autocomplete="off"  dir="rtl" placeholder="ساعت دقیق اتمام نوبت">
                    @if($errors->has('end'))
                        <p class="custom-error">{{ $errors->first('end') }}</p>
                    @endif
                </div>
                <div class="col-md-6 col-12 form-group">
                    <label for="capacity">ظرفیت :</label>
                    <input type="number" id="capacity" name="capacity" class="form-control form-control-sm" value="{{ old('capacity') }}" autocomplete="off"  dir="rtl" placeholder="ظرفیت افراد آزمایش دهنده در نوبت">
                    @if($errors->has('capacity'))
                        <p class="custom-error">{{ $errors->first('capacity') }}</p>
                    @endif
                </div>
                <div class="col-12 form-group text-left">
                    <input type="submit" class="btn btn-danger rounded-0" value="ثبت">
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/wickedpicker.js') }}"></script>
    <script src="{{ asset('js/jalali.js') }}"></script>
    <script src="{{ asset('js/calendar.js') }}"></script>
    <script src="{{ asset('js/calendar-setup.js') }}"></script>
    <script src="{{ asset('js/calendar-fa.js') }}"></script>
    <script type="text/javascript">
        $("input[name='start'] , input[name='end']").wickedpicker({
            twentyFour: true,
            minutesInterval : 5,
            timeSeparator: ':',
        });
        Calendar.setup({
            inputField     :    "date",   // id of the input field
            button         :    "date",   // trigger for the calendar (button ID)
            ifFormat       :    "%Y-%m-%d",       // format of the input field
            dateType	   :	'jalali',
            weekNumbers    : false
        });
    </script>
@endsection
@extends('layouts.admin.template')

@section('title','سانس ها')

@section('style')
    <link type="text/css" rel="stylesheet" href="{{ asset('style/index/flaticon.css') }}">
    <style>
        .table th {
            font-size: 13px;
        }

        .box {
            text-align: right;
            direction: rtl;
            border: 1px solid rgb(31, 186, 180);
            border-radius: 5px;
            line-height: 1.75;
            padding: .75rem;
            background-color: rgb(213, 251, 245);
            color: rgb(1, 104, 152);
            font-size: 14px;
            font-family: iransans_web;
            margin: 1rem 0;
        }

        .box-header, .box-header > div {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            flex-wrap: wrap;
        }

        .box-divider {
            margin: 1rem 0;
            width: 100%;
            height: 1.5px;
            background-color: rgb(31, 186, 180);
        }
    </style>
@endsection

{{--@section('content')--}}
{{--<div class="col-md-11 col-12 text-right mx-auto">--}}
{{--<div class="d-flex flex-row justify-content-between">--}}
{{--<a href="{{ route('round.show',\App\Round::find($id)->date) }}" class="shortcut-link">--}}
{{--<img src="{{ asset('icons1/return.png') }}">--}}
{{--</a>--}}
{{--<h6 class="border-bottom border-info p-2 text-primary iransans-web-medium">--}}
{{--نوبت های رزرو شده برای تاریخ‌--}}
{{--<br>--}}
{{--<span dir="rtl">{{ toFaDigits(\Morilog\Jalali\Jalalian::forge(\App\Round::find($id)->date)->format('%d %B %y')) }}</span>--}}
{{--</h6>--}}
{{--</div>--}}
{{--<div class="d-flex flex-row flex-wrap justify-content-between">--}}
{{--@foreach($users as $user)--}}
{{--<div class="box col-md-3 col-sm-6 col-12">--}}
{{--<div class="box-header">--}}
{{--<div class="mx-2"> {{ $user->name }}</div>--}}
{{--<div class="mx-2">--}}
{{--<span class="flaticon flaticon-calendar-2 ml-1"></span>--}}
{{--<span>{{ toFaDigits(\Morilog\Jalali\Jalalian::forge($user->round->date)->format('%d %B %y')) }}</span>--}}
{{--</div>--}}
{{--<div class="mx-2">--}}
{{--<i class="flaticon flaticon-clock ml-1"></i>--}}
{{--<span>{{ toFaDigits($user->round->end.' - '.$user->round->start) }}</span>--}}
{{--</div>--}}
{{--<div class="mx-2">--}}
{{--<i class="flaticon flaticon-user ml-1"></i>--}}
{{--<span>{{ toFaDigits($user->parent) }}</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="box-divider"></div>--}}
{{--<p class="mb-1">--}}
{{--<span class="font-weight-bold text-danger w-50"><span--}}
{{--class="flaticon flaticon-phone-receiver ml-1"></span>تماس</span>--}}
{{--<span class="text-dark">{{ toFaDigits($user->phone) }}</span>--}}
{{--</p>--}}
{{--<p class="mb-0">--}}
{{--<span class="font-weight-bold text-danger w-50"><span--}}
{{--class="flaticon flaticon-question ml-1"></span>سن</span>--}}
{{--<span class="text-dark mr-3">{{ toFaDigits($user->age) }}</span>--}}
{{--</p>--}}
{{--</div>--}}
{{--@endforeach--}}
{{--</div>--}}
{{--</div>--}}
{{--@endsection--}}


@section('content')
    @include('layouts.alert')

    <div class="col-12 mx-auto my-5 iransans-web-light text-right">
        <div class="d-flex flex-row justify-content-between align-items-end mb-md-4">
            <div>
                <a href="{{ route('round.show',$round->date) }}" class="shortcut-link">
                    <img src="{{ asset('icons1/return.png') }}" width="20" height="20">
                </a>
                <a href="{{ route('round.create') }}" class="btn btn-sm btn-primary px-4">
                    سانس جدید
                </a>
            </div>
            <div class="col-md-4 pr-0">
                <h5 class="mb-0">سانس <strong
                            style="font-size: 16px">({{toFaDigits(\Morilog\Jalali\Jalalian::forge($round->date)->format('%A, %d %B %y').', ساعت '.$round->end.' - '.$round->start)}}
                        )</strong></h5>
                {{--<div class="input-group input-group-sm rounded">--}}
                {{--<div class="input-group-append bg-white border border-right-0 rounded-left">--}}
                {{--<img src="{{ asset('icons1/search.png') }}" width="20" height="20" class="rounded mt-1 mx-1">--}}
                {{--</div>--}}
                {{--<input class="form-control border-left-0" type="search" placeholder="جست و جو" aria-label="Search"--}}
                {{--dir="rtl">--}}
                {{--</div>--}}
            </div>
        </div>
        @if(count($users))
            <table id="example1" class="table rounded bg-white text-dark rounded">
                <thead>
                <tr dir="rtl" class="ml-auto">
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0 d-flex align-items-center">
                        <input type="checkbox" class="toggle" value="1">
                        <span class="fas fa-chevron-down font-s font-weight-bold text-dark mr-2"
                              data-toggle="dropdown"></span>
                        <ul class="dropdown-menu dropdown-menu-right p-1 text-right">
                            <li>
                                <form id="round_multiple" method="post"
                                      action="{{ route('round.users.destroy.multiple') }}" class="d-none">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                                <a onclick="$(this).addClass('active');$('.alert-container').fadeIn();"
                                   class="delete text-dark" href="#">حذف</a>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr class="bg-light">
                    <th class="p-2" style="width: 35px"></th>
                    <th class="p-2" style="width: 35px"></th>
                    <th class="p-2" style="width: 35px"></th>
                    <th class="p-2">نتیجه آزمایش</th>
                    <th class="p-2">آدرس</th>
                    <th class="p-2">سن</th>
                    <th class="p-2">کد ملی</th>
                    <th class="p-2">شماره تماس</th>
                    <th class="p-2">
                        <div class="d-flex justify-content-end align-items-center">
                            <span class="mr-1">نام پدر</span>
                            <span class="fas fa-sort text-lighter"></span>
                        </div>
                    </th>
                    <th class="p-2">
                        <div class="d-flex justify-content-end align-items-center">
                            <span class="mr-1">نام و نام خانوادگی</span>
                            <span class="fas fa-sort text-lighter"></span>
                        </div>
                    </th>
                    <th class="p-2"></th>
                </tr>
                </thead>
                <tbody>

                @php  $index=1; @endphp
                @foreach($users as $user)
                    <tr>
                        <td>
                            @if(\App\Attachment::where('round_id',$round->id)->where('user_id',$user->id)->count() > 0)
                                <form method="post" action="{{ route('attachment.destroy' , ['round_id' => $round->id , 'user_id' => $user->id]) }}" class="d-none">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                            @endif
                            <a onclick="$(this).addClass('active');$('.alert-container').fadeIn();" href="#"
                               class="material-icons md-tooltip md-tooltip--top delete" data-md-tooltip="حذف نتیجه">
                                <img src="{{ asset('icons1/trash.png') }}" width="20" height="20">
                            </a></td>
                        <td>
                            <form method="post" action="{{ route('attachment.update',['round_id' => $round->id , 'user_id' => $user->id]) }}"
                                  enctype="multipart/form-data" class="d-none">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="file" name="file">
                            </form>
                            <a class="update material-icons md-tooltip md-tooltip--top" data-md-tooltip="ویرایش نتیجه">
                                <img src="{{ asset('icons1/edit.png') }}" width="20" height="20">
                            </a></td>
                        @if(\App\Attachment::where('round_id',$round->id)->where('user_id',$user->id)->first())
                            <td><a href="{{ route('attachment.download' , ['round_id' => $round->id , 'user_id' => $user->id]) }}"
                                   class="material-icons md-tooltip md-tooltip--top" data-md-tooltip="مشاهده نتیجه">
                                    <img src="{{ asset('icons1/eye.png') }}" width="20" height="20">
                                </a></td>
                        @else
                            <td>
                                <form method="post" action="{{ route('attachment.store',['round_id' => $round->id , 'user_id' => $user->id]) }}"
                                      enctype="multipart/form-data" class="d-none">
                                    {{ csrf_field() }}
                                    <input type="file" name="file">
                                </form>
                                <a class="upload material-icons md-tooltip md-tooltip--top"
                                   data-md-tooltip="آپلود نتیجه">
                                    <img src="{{ asset('icons1/upload.png') }}" width="20" height="20">
                                </a></td>
                        @endif
                        <td class="text-dark">
                            <span class="badge badge-{{ \App\Attachment::where('round_id',$round->id)->where('user_id',$user->id)->first() ? 'success' : 'warning' }}">{{ \App\Attachment::where('round_id',$round->id)->where('user_id',$user->id)->first() ? 'ارسال شده' : 'در انتظار' }}</span>
                        </td>
                        <td class="text-dark font-s">{{ toFaDigits($user->address) }}</td>
                        <td class="text-dark">{{ toFaDigits($user->age) }}</td>
                        <td class="text-dark">{{ toFaDigits($user->melli_code) }}</td>
                        <td class="text-dark">{{ toFaDigits($user->phone) }}</td>
                        <td class="text-dark" dir="rtl">{{ toFaDigits($user->parent) }}</td>
                        <td class="text-dark">{{ toFaDigits($user->name) }}</td>
                        <td style="width: 15px">
                            <input type="checkbox" name="deletable[]" value="{{ $user->id }}">
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-primary text-right my-3" dir="rtl">
این سانس توسط کسی رزرو نشده است.
            </div>
        @endif
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("a.upload , a.update").on("click", function () {
                $(this).prev().find('input[type=\'file\']').click();
            });

            $("input[type='file']").on("change", function () {
                $(this).parent().submit();
            })
        });
    </script>
@endsection
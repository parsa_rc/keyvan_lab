@extends('layouts.admin.template')

@section('title','سانس ها')

@section('style')
    <style>
        table.table th {
            border: none;
        }

        .text-lighter {
            color: #989895;
        }

        /**[title]::{*/
        /*font-family: iransans_web;*/
        /*}*/
    </style>
@endsection

@section('content')

    @include('layouts.alert')

    <div class="col-md-8 col-12 mx-auto my-5 iransans-web-light text-right">
        <div class="d-flex flex-row justify-content-between align-items-end mb-md-4">
            <div>
                <a href="{{ route('round.index') }}" class="shortcut-link">
                    <img src="{{ asset('icons1/return.png') }}" width="20" height="20">
                </a>
                <a href="{{ route('round.create') }}" class="btn btn-sm btn-primary px-4">
                    سانس جدید
                </a>
            </div>
            <div class="col-md-4 pr-0">
                @if(count($rounds))
                    <h5 class="mb-0">سانس <strong style="font-size: 16px">({{toFaDigits(\Morilog\Jalali\Jalalian::forge($rounds[0]->date)->format('%A, %d %B %y'))}})</strong></h5>
                @endif
                {{--<div class="input-group input-group-sm rounded">--}}
                    {{--<div class="input-group-append bg-white border border-right-0 rounded-left">--}}
                        {{--<img src="{{ asset('icons1/search.png') }}" width="20" height="20" class="rounded mt-1 mx-1">--}}
                    {{--</div>--}}
                    {{--<input class="form-control border-left-0" type="search" placeholder="جست و جو" aria-label="Search"--}}
                           {{--dir="rtl">--}}
                {{--</div>--}}
            </div>
        </div>
        @if(count($rounds))
            <table id="example1" class="table rounded bg-white text-dark rounded">
                <thead>
                <tr dir="rtl" class="ml-auto">
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                    <td class="border-0 d-flex align-items-center">
                        <input type="checkbox" class="toggle" value="1">
                        <span class="fas fa-chevron-down font-s font-weight-bold text-dark mr-2"
                              data-toggle="dropdown"></span>
                        <ul class="dropdown-menu dropdown-menu-right p-1 text-right">
                            <li>
                                <form id="round_multiple" method="post"
                                      action="{{ route('round.destroy.multiple') }}" class="d-none">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                                <a onclick="$(this).addClass('active');$('.alert-container').fadeIn();"
                                   class="delete text-dark" href="#">حذف</a>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr class="bg-light">
                    <th class="p-2" style="width: 35px"></th>
                    <th class="p-2" style="width: 35px"></th>
                    <th class="p-2" style="width: 35px"></th>
                    <th class="p-2">
                        <div class="d-flex justify-content-end align-items-center">
                            <span class="mr-1">باقی مانده</span>
                            <span class="fas fa-sort text-lighter"></span>
                        </div>
                    </th>
                    <th class="p-2">
                        <div class="d-flex justify-content-end align-items-center">
                            <span class="mr-1">ظرفیت</span>
                            <span class="fas fa-sort text-lighter"></span>
                        </div>
                    </th>
                    <th class="p-2">ساعت</th>
                    <th class="p-2"></th>
                </tr>
                </thead>
                <tbody>

                @php  $index=1; @endphp
                @php $rounds = \App\Round::where('date' , $rounds[0]->date)->get();
                        $rounds=collect($rounds)->sort(function ($a,$b){
         $int_a = intval(str_replace(':','',$a['start']).str_replace(':','',$a['end']));
         $int_b = intval(str_replace(':','',$b['start']).str_replace(':','',$b['end']));
         if ($int_a == $int_b){
             return 0;
         }
         return ($int_a < $int_b) ? -1 : 1;
     });
                @endphp
                @foreach($rounds as $round)
                    <tr>
                        <td>
                            <form id="round_{{ $round->id }}" method="post"
                                  action="{{ route('round.destroy' , $round->id) }}" class="d-none">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                            </form>
                            <a onclick="$(this).addClass('active');$('.alert-container').fadeIn();" href="#"
                               class="material-icons md-tooltip md-tooltip--top delete" data-md-tooltip="حذف">
                                <img src="{{ asset('icons1/trash.png') }}" width="20" height="20">
                            </a></td>
                        <td><a href="{{ route('round.edit' , $round->id) }}"
                               class="material-icons md-tooltip md-tooltip--top" data-md-tooltip="ویرایش">
                                <img src="{{ asset('icons1/edit.png') }}" width="20" height="20">
                            </a></td>
                        <td><a href="{{ route('user.round' , $round->id) }}"
                               class="material-icons md-tooltip md-tooltip--top" data-md-tooltip="مشاهده رزروها">
                                <img src="{{ asset('icons1/eye.png') }}" width="20" height="20">
                            </a></td>
                        <td class="text-dark">{{ toFaDigits($round->remaining) }}</td>
                        <td class="text-dark" dir="rtl">{{ toFaDigits($round->capacity) }}</td>
                        <td class="text-dark">{{ toFaDigits($round->start.' - '.$round->end) }}</td>
                        <td style="width: 15px">
                            <input type="checkbox" name="deletable[]" value="{{ $round->date }}">
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-primary text-right my-3" dir="rtl">
                سانسی برای نمایش وجود ندارد.
            </div>
        @endif
    </div>

@endsection

@section('script')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#example1").DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": false,
            });

            $("input[type='checkbox'].toggle").on("change", function () {
                $("input[type='checkbox']").prop("checked", $(this).prop("checked"));
            });

            $("#round_multiple").on("submit", function (e) {
                e.preventDefault();
                var form = document.getElementById("round_multiple");
                var ids = document.getElementsByName("deletable[]");
                var _ids = [];
                for (var i = 0; i < ids.length; i++) {
                    if (ids[i].checked) {
                        _ids.push(ids[i].value);
                    }
                }
                $.ajax({
                    type: "delete",
                    url: form.action,
                    data: {
                        "deletable": _ids
                    },
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (response) {
                        console.log(response);
                        window.location.href = "{{ url()->current() }}";
                    }, error: function (xhr) {
                        console.log(xhr);
                    }
                });
            });
        });
    </script>
@endsection
@extends('layouts.app')

@section('title','رزرو نوبت')

@section('style')
    <style>
        .custom-error {
            text-align: right;
            direction: rtl;
            font-weight: bold;
            font-size: 12px;
            color: #e36763;
        }
        .btn-disabled{
            opacity: .6;
            cursor: not-allowed !important;
        }
    </style>
@endsection

@section('content')
    <div class="w-100" style="overflow-x: hidden">
        @include('layouts.header')
        <hr class="p-0 m-0 bg-secondary">
        <section class="contact-page-section" style="background-image:url(/images/background/25.jpg)">

            <div class="pattern-layer" style="background-image:url(/images/background/pattern-13.png)"></div>
            <div class="container">
                <div class="row">
                    <!-- Form Column -->
                    <div class="form-column col-lg-5 col-md-6 col-12">
                        <div class="inner-column">
                            <div class="title-box">
                                <div class="title">زمان انتخاب شده جهت
<br>
                                    آزمایش ویروس کرونا
                                </div>
                                <br>
                                {{--<h3>برای ما بنویسید</h3>--}}
                                {{--<h6 class="text-white" dir="rtl">رزرو برای تاریخ :--}}
                                <h6 class="text-warning">
                                    {{ toFaDigits(\Morilog\Jalali\Jalalian::forge($round->date)->format('%A, %d %B %y')) }}
                                    <br>
                                    {{ toFaDigits($round->start.' - '.$round->end) }}
                                    {{--</h6>--}}
                                </h6>
                            </div>

                            <!-- Contact Form -->
                            <div class="contact-form">
                                <form method="post" id="register_form" action="{{ route('user.store',$round->id) }}">
                                    {{ csrf_field() }}
                                    <div id="form_section_phone" class="form-section">
                                        <div class="form-group">
                                            <span class="icon icon-phone-call3"></span>
                                            <input type="text" name="phone" value="{{ old('phone') }}"
                                                   autocomplete="off"
                                                   placeholder="شماره تماس ">
                                            @if($errors->has('phone'))
                                                <p class="custom-error">{{ $errors->first('phone') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button id="otp_send_btn" class="theme-btn submit-btn" type="button"
                                                    name="submit-form">ارسال کد تایید
                                            </button>
                                        </div>
                                    </div>
                                    <div id="form_section_code" class="form-section d-none">
                                        <div class="form-group">
                                            <span class="icon icon-message-2"></span>
                                            <input type="text" name="code" autocomplete="off"
                                                   placeholder="کد تایید یک بار مصرف">
                                            @if($errors->has('code'))
                                                <p class="custom-error">{{ $errors->first('code') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button id="otp_verify_btn" class="theme-btn submit-btn" type="button"
                                                    name="submit-form">
                                                تایید کد
                                            </button>
                                            <button id="otp_resend_btn" dir="rtl" class="theme-btn submit-btn btn-disabled"
                                                    type="button"
                                                    name="submit-form">
                                                ارسال مجدد
                                                <span class="countdown-timer"></span>
                                            </button>
                                        </div>
                                    </div>
                                    <div id="form_section_data" class="form-section d-none">
                                        <div class="form-group">
                                            <span class="icon flaticon-user-2"></span>
                                            <input type="text" name="name" autocomplete="off" value="{{ old('name') }}"
                                                   placeholder="نام و نام خانوادگی ">
                                            @if($errors->has('name'))
                                                <p class="custom-error">{{ $errors->first('name') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <span class="icon flaticon-user-2"></span>
                                            <input type="text" name="parent" autocomplete="off"
                                                   value="{{ old('parent') }}"
                                                   placeholder="نام پدر ">
                                            @if($errors->has('parent'))
                                                <p class="custom-error">{{ $errors->first('parent') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <span class="icon icon-code"></span>
                                            <input type="text" name="melli_code" autocomplete="off"
                                                   value="{{ old('melli_code') }}" placeholder="کد ملی ۱۰ رقمی ">
                                            @if($errors->has('melli_code'))
                                                <p class="custom-error">{{ $errors->first('melli_code') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <span class="icon icon-question-circle"></span>
                                            <input type="text" name="age" autocomplete="off" value="{{ old('age') }}"
                                                   placeholder="سن ">
                                            @if($errors->has('age'))
                                                <p class="custom-error">{{ $errors->first('age') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <span class="icon icon-map-marker2"></span>
                                            <textarea type="text" name="address" autocomplete="off"
                                                      placeholder="آدرس محل سکونت را وارد کنید">{{ old('address') }}</textarea>
                                            @if($errors->has('address'))
                                                <p class="custom-error">{{ $errors->first('address') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button id="submit_btn" class="theme-btn submit-btn" type="button"
                                                    name="submit-form">ثبت
                                                اطلاعات
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group text-right text-white mb-0">
                                        <a href="{{ route('user.login',$round->id) }}"
                                           class="">قبلا نوبت گرفته اید ؟</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Info Column -->
                    <div class="info-column col-lg-7 col-md-6 col-12">
                        <div class="inner-column">
                            <!-- Sec Title -->
                            <div class="section-title">
                                <h2>قوانین و شریط پذیرش آنلاین</h2>
                                <p class="text text-justify" style="font-size: 14px;font-family: iransans_web" dir="rtl">
                                    با در نظر گرفتن شرایط خاص کنونی و حساسیت آزمایش ویروس کرونا خواهشمند است پس از ثبت نام، فقط در زمان
                                    مقرری که برای شما ثبت و توسط پیامک ارسال گردیده است در محل پذیرش آزمایشگاه کیوان واقع در آدرس زیر حاضر
                                    شده و کد پذیرش خود را دریافت نمایید.
                                    در غیر این صورت، نوبت نمونه گیری شما به صورت اتوماتیک لغو شده و <strong style="font-family: iransans_web_bold">زمان
                                        جایگزینی برای پذیرش و نمونه گیری از شما در نظر گرفته نمی‌شود</strong>. بدیهی است برای دریافت زمان جدید ملزم
                                    به ثبت نام مجدد و پرداخت هزینه آزمایش می‌باشد.
                                    <br>لازم به تاکید است که شما با تکمیل فرم پیش رو و پرداخت مبلغ آزمایش، ضمن گواهی بر صحت موارد ذکر شده در فرم
                                    ارسالی و برعهده گرفتن کلیه عواقب قانونی آن، موافقت خود را با شرایط فوق الذکر اعلام می‌نمایید.
                                </p>
                            </div>
                            <div class="row">
                                <!-- Column -->
                                <div class="column col-sm-6 col-12">
                                    <div class="contact-address">
                                        <div class="inner">
                                            <div class="icon-box">
                                                <span class="icon flaticon-map-pin-marked"></span>
                                            </div>
                                            <h4>آدرس محل نمونه گیری</h4>
                                            <p class="text">
                                                تهران، خیابان شهید بهشتی (عباس‌آباد)، بعد از سینما آزادی، به طرف خیابان
                                                ولیعصر، پلاک ۴۹۸ طبقه همکف
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Column -->
                                <div class="column col-sm-6 col-12">
                                    <div class="contact-address">
                                        <div class="inner">
                                            <div class="icon-box">
                                                <span class="icon flaticon-clock"></span>
                                            </div>
                                            <h4>زمان مراجعه</h4>
                                            <p class="text">
                                               لطفا جهت جلوگیری از سرایت ویروس و حفظ فاصله گذاری
                                                اجتماعی فقط در زمان مقرر مراجعه فرمایید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Column -->
                                <div class="column col-sm-6 col-12">
                                    <div class="contact-address">
                                        <div class="inner">
                                            <div class="icon-box">
                                                <span class="icon icon-money"></span>
                                                {{--<span class="icon flaticon-clock"></span>--}}
                                            </div>
                                            <h4>هزینه آزمایش</h4>
                                            <p class="text">
                                                هزینه انجام هر تست کرونا با تخفیف آزمایشگاه
                                                کیوان، مبلغ ۲,۶۰۰,۰۰۰ ریال می‌باشد.(تعرفه مصوبه دولت ۴,۷۶۱,۰۰۰ ریال است)
                                                در حال حاضر این آزمایش تحت پوشش سازمان‌های بیمه‌گر پایه نمی‌باشد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Column -->
                                <div class="column col-sm-6 col-12">
                                    <div class="contact-address">
                                        <div class="inner">
                                            <div class="icon-box">
                                                <span class="icon flaticon-letter"></span>
                                            </div>
                                            <h4>جوابدهی آنلاین
                                               </h4>
                                            <p class="text">شما می‌توانید جواب آزمایش خود را پس از ۳ روز کاری در همین سامانه به صورت
                                                نسخه دیجیتال و یا در صورت تمایل با مراجعه به پذیرش آزمایشگاه بصورت نسخه
                                                چاپی دریافت نمایید.
                                                ضمناً در صورت پرداخت هزینه پیک، نسخه چاپی برای شما ارسال می‌گردد.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('footer')
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            var timeCurrent = 90, countDownTimer;

            function generateTimer() {
                timeCurrent--;
                if (timeCurrent >= 0) {
                    var min = parseInt(timeCurrent / 60);
                    var sec = parseInt(timeCurrent % 60);
                    min = min >= 10 ? min : ('0' + min);
                    sec = sec >= 10 ? sec : ('0' + sec);
                    $(".countdown-timer").text("(" + min + ":" + sec + ")");
                } else {
                    clearInterval(countDownTimer);
                    {{ session()->remove('otp') }}
                    $("input[name='code']").siblings(".custom-error").remove();
                    $("input[name='code']").after("<p class='custom-error'>مدت زمان اعتبار سنجی کد شما به پایان رسیده است, لطفا ارسال مجدد را انتخاب کنید</p>");
                    $("#otp_resend_btn").removeClass("btn-disabled");
                    $("#otp_verify_btn").addClass("btn-disabled");
                    $(".countdown-timer").hide();
                }
            }

            $("#otp_send_btn , #otp_resend_btn").on("click", function () {
                var cur = $(this);
                if (!cur.hasClass("btn-disabled")){
                    $("input[name='code']").siblings(".custom-error").remove();
                    $("#otp_resend_btn").addClass("btn-disabled");
                    $("#otp_verify_btn").removeClass("btn-disabled");
                    if (cur.is($("#otp_resend_btn"))){
                        timeCurrent=90;
                        console.log("resend");
                        $(".countdown-timer").show();
                        countDownTimer = setInterval(generateTimer, 1000);
                    }
                    $.ajax({
                        async: false,
                        method: "POST",
                        url: "{{ route('user.otp.send') }}",
                        data: {
                            phone: $("input[name='phone']").val(),
                            route: '{{ \Illuminate\Support\Facades\Route::currentRouteName() }}'
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            if(cur.is($("#otp_send_btn"))){
                                console.log(response);
                                $("#form_section_phone").addClass("d-none");
                                $("#form_section_code").removeClass("d-none");
                                $(".countdown-timer").show();
                                countDownTimer = setInterval(generateTimer, 1000);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr);
                            if (xhr.status == 429) {
                                if(cur.is($("#otp_send_btn"))){
                                    $("input[name='phone']").siblings(".custom-error").remove();
                                    $("input[name='phone']").after('<p class=\'custom-error\'>' + xhr.responseJSON.phone + '</p>')
                                }else{
                                    $("input[name='code']").siblings(".custom-error").remove();
                                    $("input[name='code']").after('<p class=\'custom-error\'>' + xhr.responseJSON.phone + '</p>')
                                }
                            }else {
                                $.each(xhr.responseJSON.errors, function (key, value) {
                                    $("input[name='" + key + "']").siblings(".custom-error").remove();
                                    $("input[name='" + key + "']").after('<p class="custom-error">' + value + '</p>');
                                });
                            }
                        }
                    });
                }
            });
            $("#otp_verify_btn").on("click", function () {
                if (!$(this).hasClass("btn-disabled")){
                    $.ajax({
                        async: false,
                        method: "POST",
                        url: "{{ route('user.otp.verify') }}",
                        data: {code: $("input[name='code']").val()},
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            $("#form_section_code").addClass("d-none");
                            $("#form_section_data").removeClass("d-none");
                        },
                        error: function (xhr) {
                            console.log(xhr.responseJSON.code);
                            if (xhr.status == 404) {
                                $("input[name='code']").siblings(".custom-error").remove();
                                $("input[name='code']").after('<p class="custom-error">' + xhr.responseJSON.code + '</p>');
                            } else {
                                $.each(xhr.responseJSON.errors, function (key, value) {
                                    $("input[name='" + key + "']").siblings(".custom-error").remove();
                                    $("input[name='" + key + "']").after('<p class="custom-error">' + value + '</p>');
                                });
                            }
                        }
                    });
                }
            });
            $("#submit_btn").on("click", function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('user.prestore',$round->id) }}",
                    data: {
                        phone: $("input[name='phone']").val(),
                        name: $("input[name='name']").val(),
                        parent: $("input[name='parent']").val(),
                        melli_code: $("input[name='melli_code']").val(),
                        address: $("[name='address']").val(),
                        age: $("input[name='age']").val(),
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        $("#register_form").submit();
                    },
                    error: function (xhr) {
                        console.log(xhr.responseJSON.code);
                        if (xhr.status == 404) {
                            $("input[name='code']").siblings(".custom-error").remove();
                            $("input[name='code']").parent().remove(".custom-error").append('<p class="custom-error">' + xhr.responseJSON.code + '</p>');
                        } else {
                            $.each(xhr.responseJSON.errors, function (key, value) {
                                $("[name='" + key + "']").siblings(".custom-error").remove();
                                $("[name='" + key + "']").parent().remove(".custom-error").append('<p class="custom-error">' + value + '</p>');
                            });
                        }
                    }
                })
                $("input").on("focus", function () {
                    $(this).next(".custom-error").remove();
                });
            });
        });
    </script>
@endsection
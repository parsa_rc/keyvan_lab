@extends('layouts.app')

@section('style')
    <style>
        body{
            background: rgb(244, 244, 244);
        }
    </style>
@endsection

@section('content')

    @foreach($errors->all() as $err)
        <li>{{ $err }}</li>
    @endforeach

    <div class="col-md-4 mx-auto mt-5">
        <div class="panel iransans-web">
            <div class="panel-header text-center my-3">
                <h4>
                    <span>آزمایشگاه ویروس شناسی کیوان</span>
                </h4>
            </div>
            <div class="panel-body bg-white rounded shadow-sm p-4 text-center">
                <h4 class="vazir font-weight-bold">!خوش      آمدید</h4>
                <hr class="mx-5 my-4">
                <form method="post" action="{{ route('admin.login.check') }}" class="text-right vazir">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email">ایمیل</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control form-control-sm" autocomplete="off" dir="rtl">
                        @if($errors->has('email'))
                            <p class="text-right font-s text-danger font-weight-bold"
                               dir="rtl">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pwd">رمز عبور</label>
                        <input id="pwd" type="password" name="password" class="form-control form-control-sm" autocomplete="off" dir="rtl">
                    </div>
                    {{--<a href="#" class="text-dark iransans-web font-s pb-3">فراموشی رمزعبور؟</a>--}}
                    <div class="form-group mt-2">
                        <input type="submit" class="btn btn-sm btn-block btn-primary" value="ورود">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
<div class="col-md-10 col-11 mx-auto my-md-5 my-3">
    <div class="your-class">
        @for($i=0;$i<14;$i++)
            <div>
                <div class="box box-header">
                    @php $carbon_date = \Carbon\Carbon::now()->add('day',$i); @endphp
                    @php $date = \Morilog\Jalali\Jalalian::forge($carbon_date); @endphp
                    <h6 class="box-title">{{ $date->format('%A') }}</h6>
                    <p class="box-date">{{ toFaDigits($date->format('%d %B %Y')) }}</p>
                    @php $rounds = \App\Round::where('date' , $carbon_date->toDateString())->get();
                        $rounds=collect($rounds)->sort(function ($a,$b){
         $int_a = intval(str_replace(':','',$a['start']).str_replace(':','',$a['end']));
         $int_b = intval(str_replace(':','',$b['start']).str_replace(':','',$b['end']));
         if ($int_a == $int_b){
             return 0;
         }
         return ($int_a < $int_b) ? -1 : 1;
     });
                    @endphp
                </div>
                @forelse($rounds as $round)
                    <a href="{{ $round->remaining > 0 ? (auth('web')->check() ? route('user.pay.start',$round->id) : route('user.join',$round->id)) : '' }}"
                       style="text-decoration: none;cursor: {{ $round->remaining > 0 ? 'pointer' : 'not-allowed' }}">
                        <div class="box box-content">
                            <p class="p-2">{{ toFaDigits($round->end.' - '.$round->start) }}</p>
                            <p class="box-date">
                                <span class="badge badge-primary">ظرفیت</span>
                                <strong>{{ toFaDigits($round->capacity) }}</strong>
                            </p>
                            <p class="box-date">
                                <span class="badge badge-warning">مانده</span>
                                <strong>{{ toFaDigits($round->remaining) }}</strong>
                            </p>
                        </div>
                    </a>
                @empty
                    <p class="iransans-web font-s text-center" dir="rtl">
                        سانسی
                        برای امروز
                        تعریف
                        نشده است.
                    </p>
                @endforelse
            </div>
        @endfor
    </div>
</div>

<footer id="contact-us" class="main-footer" style="background-image:url(/images/zbackground/7.jpg)">

    <div class="container">

        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row">

                <!-- Logo Widget-->
                <div class="footer-column col-lg-6 col-md-6">
                    <p class="text text-justify">
                        آزمایشگاه کیوان به دلیل ارائه خدماتی با کیفیت بالا، با
                        مؤسسات دولتی و خصوصی و مراکز آموزش عالی در
                        حال همکاری و در طرحهای تحقیقاتی بسیاری شرکت
                        می‌نماید. این آزمایشگاه تست‌های اورژانس روی
                        نمونه‌های دریافتی را ظرف مدت ۲۴ ساعت پس از
                        دریافت نمونه، آماده و نتایج را اعلام می‌نماید.
                    </p>
                </div>
                <div class="footer-column col-lg-6 col-md-6">
                    <div class="footer-widget logo-widget">
                        <div class="logo">
                            <a href="#"><img src="/images/zfooter-logo-a.png" alt=""/></a>
                        </div>
                        <ul class="list-style-two">
                            <li><a href="#map-location"><span
                                            class="icon flaticon-map-pin-marked"></span>تهران، خیابان شهید بهشتی (عباس‌آباد)، بعد از سینما آزادی، به طرف خیابان ولیعصر، پلاک ۴۹۸ طبقه همکف</a></li>
                            <li><a href="#"><span class="icon flaticon-phone"></span>۰۲۱-۸۸۷۰۶۵۵۶ ۰۲۱-۸۸۷۱۲۸۳۵ ۰۲۱-۸۸۷۱۵۳۵۰ ۰۲۱-۸۸۷۰۹۵۱۳ ۰۲۱-۸۸۱۰۴۸۹۳ ۰۲۱-۸۸۱۰۴۹۵۴</a>
                            </li>
                            <li><a href="mailto:info@keyvanlabtest.ir"><span class="icon flaticon-mail"></span>info@keyvanlabtest.ir</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="clearfix">

                <div class="pull-right ">
                    <!-- Copyright -->
                    <p class="copyright">
                        <span>&copy; 1398 تمامی حقوق محفوظ است.</span>
                        {{--<a href="http://virtopia.ir">طراحی و اجرا :--}}
                            {{--<img src="{{ asset('images/virtopia.png') }}" style="width: 200px;height: 100px">--}}
                        {{--</a>--}}
                    </p>
                </div>

                <div class="pull-left">
                    <ul class="social-box">
                        <li class="messanger"><a href="https://keyvanlab.com/"><span class="icon flaticon-messenger"></span>
                                آزمایشگاه تخصصی ویروس شناسی کیوان</a></li>
                        <li class="facebook"><a href="https://www.facebook.com/keyvan.lab.92" class="icon icon-facebook"></a></li>
                        <li class="linkedin"><a href="https://www.linkedin.com/in/keyvan-lab-74a912184/" class="icon icon-linkedin"></a></li>
                        <li class="instagram"><a href="https://www.instagram.com/keyvanlabtest" class="icon icon-instagram"></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

</footer>

<nav class="navbar navbar-expand-md d-flex flex-wrap flex-row-reverse {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'home' ? '' : '-dark' }}">
    <a href="{{ route('home') }}" class="navbar-brand ml-3 my-3 mr-0"><img src="{{ asset('icons1/logo.png') }}"></a>
    {{--<div class="elementor-flex-right">--}}
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <img src="{{ asset('icons1/toggle'.(\Illuminate\Support\Facades\Route::currentRouteName() == 'home' ? '' : '-dark').'.png') }}"
             width="28" height="28">
    </button>

    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <div class="w-100 d-flex flex-lg-row flex-column-reverse flex-wrap justify-content-between align-items-lg-center align-items-end">
            <ul class="navbar-nav d-md-block d-none text-right">
                <li class="navbar-text m-lg-2 m-1">
                    @if(auth()->check())
                        <a href="{{ route(auth()->user()->isAdmin() ? 'admin.home' :'home') }}" class="btn-custom">پنل کاربری</a>
                    @else
                        <a href="{{ route('join-member.view') }}" class="btn-custom">عضو شوید</a>
                    @endif
                </li>
                <li class="navbar-text m-lg-2 m-1">
                    <span>۰۹۰۵۷۵۰۷۹۶۹</span>
                    <img src="{{ asset('icons1/phone.png') }}">
                </li>
                <li class="navbar-text m-lg-2 m-1">
                    <span>شنبه تا پنج شنبه : 8 تا 8 شب</span>
                    <img src="{{ asset('icons1/clock.png') }}">
                </li>
            </ul>
            <ul class="navbar-nav text-right" dir="rtl">
                <li class="nav-item m-lg-2 m-1"><a href="{{ route('home') }}" class="nav-link active">خانه</a></li>
                <li class="nav-item m-lg-2 m-1"><a href="#" class="nav-link">درباره ما</a></li>
                <li class="nav-item m-lg-2 m-1"><a href="{{ route('contact.create') }}" class="nav-link">تماس با ما</a></li>
                <li class="nav-item m-lg-2 m-1 d-md-none d-block">
                    @if(auth()->check())
                        <a href="{{ route(auth()->user()->isAdmin() ? 'admin.home' :'home') }}" class="nav-link">پنل کاربری</a>
                    @else
                        <a href="{{ route('join-member.view') }}" class="btn-custom">عضو شوید</a>
                        <a href="{{ route('login-member.view') }}" class="btn-custom">ورود</a>
                    @endif
                </li>
            </ul>
        </div>
    </div>

    {{--</div>--}}
</nav>
<nav class="main-header navbar navbar-expand navbar-white navbar-dark" style="background-color: #3c8dbc">

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-3">
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i--}}
                        {{--class="fas fa-cogs"></i></a>--}}
        {{--</li>--}}

        <!-- Notifications Dropdown Menu -->
        <li id="registered_users_notification" class="nav-item dropdown">
            @component('layouts.notifications.all' , ['notifications' => auth('admin')->user()->newMembers() ])
                @slot('id') mark_as_read_users @endslot
                @slot('title') عضو جدید @endslot
                @slot('icon') far fa-bell @endslot
                @slot('badge') badge-warning @endslot
            @endcomponent
        {{--</li>--}}

        {{--<!-- Messages Dropdown Menu -->--}}
        {{--<li class="nav-item dropdown">--}}
            {{--@component('layouts.notifications.all' , ['notifications' => auth()->user()->newMessages()])--}}
                {{--@slot('id') mark_as_read_messages @endslot--}}
                {{--@slot('title') پیام جدید @endslot--}}
                {{--@slot('icon')far fa-comments @endslot--}}
                {{--@slot('badge') badge-danger @endslot--}}
            {{--@endcomponent--}}
            {{--<div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">--}}
            {{--<a href="#" class="dropdown-item">--}}
            {{--<!-- Message Start -->--}}
            {{--<div class="media">--}}
            {{--<img src="{{ asset('adminlte/dist/img/user1-128x128.jpg') }}" alt="User Avatar"--}}
            {{--class="img-size-50 mr-3 img-circle">--}}
            {{--<div class="media-body">--}}
            {{--<h3 class="dropdown-item-title">--}}
            {{--Brad Diesel--}}
            {{--<span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>--}}
            {{--</h3>--}}
            {{--<p class="text-sm">Call me whenever you can...</p>--}}
            {{--<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Message End -->--}}
            {{--</a>--}}
            {{--<div class="dropdown-divider"></div>--}}
            {{--<a href="#" class="dropdown-item">--}}
            {{--<!-- Message Start -->--}}
            {{--<div class="media">--}}
            {{--<img src="{{ asset('adminlte/dist/img/user8-128x128.jpg') }}" alt="User Avatar"--}}
            {{--class="img-size-50 img-circle mr-3">--}}
            {{--<div class="media-body">--}}
            {{--<h3 class="dropdown-item-title">--}}
            {{--John Pierce--}}
            {{--<span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>--}}
            {{--</h3>--}}
            {{--<p class="text-sm">I got your message bro</p>--}}
            {{--<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Message End -->--}}
            {{--</a>--}}
            {{--<div class="dropdown-divider"></div>--}}
            {{--<a href="#" class="dropdown-item">--}}
            {{--<!-- Message Start -->--}}
            {{--<div class="media">--}}
            {{--<img src="{{ asset('adminlte/dist/img/user3-128x128.jpg') }}" alt="User Avatar"--}}
            {{--class="img-size-50 img-circle mr-3">--}}
            {{--<div class="media-body">--}}
            {{--<h3 class="dropdown-item-title">--}}
            {{--Nora Silvester--}}
            {{--<span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>--}}
            {{--</h3>--}}
            {{--<p class="text-sm">The subject goes here</p>--}}
            {{--<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Message End -->--}}
            {{--</a>--}}
            {{--<div class="dropdown-divider"></div>--}}
            {{--<a href="#" class="dropdown-item dropdown-footer">See All Messages</a>--}}
            {{--</div>--}}
        </li>
    </ul>

    <!-- SEARCH FORM -->
    {{--<form class="form-inline ml-auto">--}}
    {{--<div class="input-group input-group-sm">--}}
    {{--<div class="input-group-append">--}}
    {{--<button class="btn btn-navbar" type="submit">--}}
    {{--<i class="fas fa-search"></i>--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--<input class="form-control form-control-navbar" type="search" placeholder="جست و جو" aria-label="Search" dir="rtl">--}}
    {{--</div>--}}
    {{--</form>--}}
    <ul class="navbar-nav ml-auto">
        {{--<li class="nav-item d-none d-sm-inline-block">--}}
        {{--<a href="#" class="nav-link">Contact</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item d-none d-sm-inline-block">--}}
        {{--<a href="#" class="nav-link">Home</a>--}}
        {{--</li>--}}
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
</nav>
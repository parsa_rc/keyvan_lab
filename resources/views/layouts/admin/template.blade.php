<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>پنل ادمین - @yield('title')</title>

    <!-- Font Awesome Icons -->
    <link type="text/css" rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link type="text/css" rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('adminlte/dist/css/custom-style.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('style/font.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('style/tooltip.css') }}">
    {{--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">--}}

    <style>
        #blueimp-gallery {
            position: fixed;
            z-index: 999999;
            bottom: 0;
            left: 0;
            right: 0;
            top: 0;
            background-color: rgba(54, 54, 54, 0.84);
        }

        #blueimp-gallery > img {
            position: absolute;
            max-width: 100%;
            max-height: 100%;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        #blueimp-gallery .close {
            position: absolute;
            top: 5%;
            right: 5%;
            color: white;
            font-size: 6rem;
            font-weight: bold;
        }

        .shortcut-link {
            /*width: 75px;*/
            /*height: 75px;*/
            margin: .5rem;
            padding: .5rem;
            border-radius: .25rem;
            -webkit-transition: ease .25s;
            -moz-transition: ease .25s;
            -ms-transition: ease .25s;
            -o-transition: ease .25s;
            transition: ease .25s;
        }

        .control-sidebar label {
            font-family: vazir-light !important;
            font-weight: lighter !important;
            font-size: 14px;
        }
    </style>

    @yield('style')

</head>
<body class="hold-transition sidebar-mini">

<div class="wrapper">
    <!-- Navbar -->
@include('layouts.admin.navbar')
<!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('admin.home') }}" class="brand-link d-flex flex-row-reverse" style="background-color: #367fa9">
            <img src="https://keyvanlab.com/HCMS-assets/img/fav.ico" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light text-center m-0">آزمایشگاه کیوان</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 pr-3 d-flex">
                <div class="info">
                    <a href="#" class="d-block">{{ auth('admin')->user()->email }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column text-right iransans-web" data-widget="treeview"
                    role="menu"
                    data-accordion="false">
                    <li class="nav-item">
                        <a href="{{ route('admin.home') }}" class="nav-link {{ strpos(\Illuminate\Support\Facades\Route::currentRouteName(),'admin.') !== false ? 'active' : '' }}">
                            <i class="nav-icon fas fa-home"></i>
                            <p>داشبورد</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('round.index') }}" class="nav-link {{ strpos(\Illuminate\Support\Facades\Route::currentRouteName(),'round.') !== false ? 'active' : '' }}">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>سانس</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('price.set') }}" class="nav-link {{ strpos(\Illuminate\Support\Facades\Route::currentRouteName(),'price.') !== false ? 'active' : '' }}">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>هزینه</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <form id="admin_logout_form" method="post" action="{{ route('admin.logout') }}" class="d-none">
                            {{ csrf_field() }}
                        </form>
                        <a href="#" onclick="$('#admin_logout_form').submit();" class="nav-link">
                            <i class="nav-icon fas fa-sign-out-alt"></i>
                            <p>خروج</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper text-center pb-5">
        <div class="col-12 d-flex flex-row justify-content-between align-items-center px-md-5 px-2 pt-1">
            <p></p>
            <ol class="breadcrumb" style="background-color: transparent">
            </ol>
        </div>
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    {{--<aside class="control-sidebar control-sidebar-dark iransans-web">--}}
        {{--<!-- Control sidebar content goes here -->--}}
        {{--<div class="col-12 text-right pr-3 pt-3" dir="rtl">--}}
            {{--به زودی در این قسمت تنظیمات پنل ادمین قرار داده میشود.--}}
        {{--</div>--}}
    {{--</aside>--}}
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
{{--<!-- AdminLTE App -->--}}
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>

<script>
    $(document).ready(function () {

        var toggle = 0;

        $("#mark_as_read_users").click(function () {
            $.ajax({
                method: "GET",
                url: "{{ route('admin.users.notifications.markAsRead') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        $(".shortcut-link").hover(function () {
            $(this).toggleClass("shadow shadow-sm");
        }, function () {
            $(this).toggleClass("shadow shadow-sm");
        });

        $("#fixed_navbar_toggle_checkbox").on("change", function () {
            $("nav.main-header").toggleClass("fixed-top");
        });
    });
</script>

@yield('script')

</body>
</html>

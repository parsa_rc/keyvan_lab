@extends('layouts.admin.template')

@section('style')
    <link type="text/css" rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 d-lg-block d-none">
                </div>
                {{--<!-- ./col -->--}}
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner text-md-right text-center">
                            <h3>{{ toFaDigits(count(auth('admin')->user()->newMembers())) }}</h3>
                            <p>کاربر جدید</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer" style="visibility: hidden">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner text-md-right text-center">
                            <h3>{{ toFaDigits(\App\User::count()) }}</h3>
                            <p>کاربر عضوشده</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="{{ route('user.all') }}" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner text-md-right text-center">
                            <h3>{{ toFaDigits(\App\Round::count()) }}</h3>
                            <p>سانس</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-body-outline"></i>
                        </div>
                        <a href="{{ route('round.index') }}" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div>
    </section>
@endsection
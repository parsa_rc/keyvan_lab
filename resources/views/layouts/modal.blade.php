@if($title = \Illuminate\Support\Facades\Session::get('flash'))
    <div class="modal fade iransans-web-light" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-right" style="font-size: 15px" dir="rtl">
                {{ toFaDigits($title) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger mr-auto" data-dismiss="modal">باشد</button>
                </div>
            </div>
        </div>
    </div>
@endif
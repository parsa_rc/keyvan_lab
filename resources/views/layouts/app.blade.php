<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>آزمایشگاه تخصصی ویروس شناسی کیوان - @yield('title')</title>
    <link rel="icon" type="image/ico" href="{{ asset('images/logo.jfif') }}">
{{----}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
{{--    <link href="{{ asset('style/index/bootstrap.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('style/index/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ asset('style/index/icomoon-icons.css')  }}" rel="stylesheet">
    <link href="{{ asset('style/index/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('style/index/flaticon.css') }}" rel="stylesheet">
    <link href="{{ asset('style/index/owl.css') }}" rel="stylesheet">
    <link href="{{ asset('style/index/animation.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('style/index/magnific-popup.css') }}">
{{--    <link href="{{ asset('style/index/jquery.fancybox.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('style/index/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('style/index/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('style/index/style.css') }}" rel="stylesheet">
    <link href="{{ asset('style/index/responsive.css') }}" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="{{ asset('style/font.css') }}">

    <style>
        #back-to-admin-panel{
            position: fixed;
            text-align: center;
            top: 2rem;
            left: 2rem;
            /*width: 5rem;*/
            /*height: 3rem;*/
            background-color: #0E9A00;
            color : #eee ;
            padding: 2.5rem 1.8rem 3rem 1.8rem;
            border-radius: 50%;
            box-shadow: 0 0 4px rgba(37, 37, 37, 0.71);
            z-index: 999999999;
            text-decoration:none;
        }


        a.btn-custom {
            color: #fff;
            background-color: #cb1313;
            border-radius: 20px;
            padding: .5rem 1.75rem !important;
            text-decoration: none;

            -webkit-transition: background-color .3s;
            -moz-transition: background-color .3s;
            -ms-transition: background-color .3s;
            -o-transition: background-color .3s;
            transition: background-color .3s;
        }

        a.btn-custom:hover {
            color: #fff;
            text-decoration: none;
            background-color: rgb(227, 80, 65);
        }

    </style>
    @yield('style')

</head>
<body class="iransans-web">

@include('layouts.modal')
<main>
    @yield('content','default content')
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="{{ asset('js/index/jquery-ui.js') }}"></script>
<script src="{{ asset('js/index/owl.js') }}"></script>
<script src="{{ asset('js/index/paroller.js') }}"></script>
<script src="{{ asset('js/index/wow.js') }}"></script>
<script src="{{ asset('js/index/main.js') }}"></script>
<script src="{{ asset('js/index/script.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#myModal").modal();
    });
</script>
@yield('script')
</body>
</html>
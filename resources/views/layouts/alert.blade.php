    <style>
    .alert-container{
        display: none;
        position: fixed;
        top: 0;
        left:0;
        right : 0;
        bottom: 0;
        background-color: rgba(62, 62, 62, 0.81);
        z-index: 9999999999;
    }

    .alert-body{
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        background-color: #efefef;
        border-radius: 5px;
        box-shadow: 0px 0px 5px rgba(160, 160, 160, 0.5);
        font-family: iransans_web_light;
        padding: 1rem;
        text-align: right;
        font-size: 14px;
    }

    .alert-title{
        color : #252525;
        font-family: iransans_web_medium;
    }
</style>

<div class="alert-container">
    <div class="alert-body">
        <h6 class="alert-title">
        </h6>
        <p class="alert-text">
            {{--{{ $txt }}--}}
            آیا از حذف این سانس مطمئن هستید ؟
        </p>
        <a href="#" onclick="$(this).parents('.alert-container').fadeOut('fast');$('a.delete.active').removeClass('active')" class="alert-nok btn btn-sm btn-default text-danger">خیر</a>
        <a href="#" onclick="$(this).parents('.alert-container').fadeOut('fast');$('a.delete.active').prev('form').submit();" class="alert-ok btn btn-sm btn-default text-primary">بله</a>
    </div>
</div>
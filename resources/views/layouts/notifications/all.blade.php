<a id="{{ $id }}" class="nav-link" data-toggle="dropdown" href="#">
    <i class="{{ $icon }}"></i>
    @if($c=count($notifications))
        <span class="badge navbar-badge {{ $badge }}">{{ $c }}</span>
    @endif
</a>
<div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                <span class="dropdown-header" dir="rtl">{{ toFaDigits(count($notifications)).' '.$title }}</span>
    <div class="dropdown-divider"></div>
    @forelse($notifications as $notification)
            @include('layouts.notifications.'.Str::snake(class_basename($notification->type)))
    @empty
        <a class="dropdown-item">اعلانی وجود ندارد</a>
    @endforelse
    <div class="dropdown-divider"></div>
</div>
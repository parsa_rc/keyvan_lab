@php $user = $notification->data['user'] @endphp
<a href="#" class="dropdown-item iransans-web font-s" dir="rtl">
    <span class="iransans-web-medium">{{ $user['name'] }}</span> ملحق شد
    <span class="float-left text-muted font-s">{{ toFaDigits(\Morilog\Jalali\Jalalian::forge($user['created_at'])->ago()) }}</span>
</a>
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<div class="template-download d-flex flex-row-reverse flex-wrap align-items-center bg-white rounded shadow-sm my-2 p-sm-2 p-1 iransans-web-light" dir="ltr">
{{--<div class="flex-grow-1 border">--}}

    <div class="col-sm-2 col-3">
  <span class="preview">
  {% if (file.thumbnailUrl) { %}
  <a class="fullscreen" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}">
  {% if (file.isPhoto) { %}
  <img src="{%=file.thumbnailUrl%}" style="max-width:80px;max-height:80px">
  {% } else { %}
  <video width="110" height="110" controls>
  <source src="{%=file.thumbnailUrl%}">
  </video>
  {% } %}
  </a>
  {% } %}
  </span>
  </div>

  <div class="file-desc col-md-7 col-sm-8 mx-auto d-flex flex-md-column flex-row-reverse justify-content-start align-items-md-stretch align-items-center">
  <div class="col-md-12 col-sm-6 text-right">
  {% if (window.innerWidth > 480 || !file.thumbnailUrl) { %}
  <p class="name w-100 mb-md-2 mb-0">
  {% if (file.url) { %}
  <a class="fullscreen" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}">{%=file.name%}</a>
  {% } else { %}
  <span>{%=file.name%}</span>
  {% } %}
  </p>
  {% } %}
  {% if (file.error) { %}
  <div><span class="label label-danger">خطا</span> {%=file.error%}</div>
  {% } %}
  </div>
{{--<div class="col-md-1"></div>--}}
    <div class="col-md-12 col-sm-6 text-md-right text-center">
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </div>
    </div>
    {{--</div>--}}

    <div class="col-md-3 col-sm-2 text-left d-flex align-items-center">
{% if (file.deleteUrl) { %}
<input type="checkbox" class="toggle" value="1" name="delete">
<button class="btn btn-sm btn-danger delete" data-type="DELETE" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
<i class="fas fa-trash-alt"></i>
<span>حذف</span>
</button>
<button class="btn btn-sm btn-success ml-1 update" data-type="PUT" data-url="{%=file.updateUrl%}">
<input type="hidden" value="0">
<i class="fas fa-edit"></i>
<span>ویرایش</span>
</button>
    {% } else { %}
    <button class="btn btn-warning cancel">
    <i class="fas fa-ban"></i>
    <span>کنسل</span>
    </button>
    {% } %}
    </div>

    </div>
    {% } %}
</script>
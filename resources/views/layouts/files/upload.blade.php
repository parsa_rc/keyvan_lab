<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<div class="template-upload d-flex flex-row-reverse flex-wrap align-items-center bg-white rounded shadow-sm my-2 p-sm-2 p-1 iransans-web-light" dir="ltr">
        <div class="col-sm-2 col-3">
        <span class="preview"></span>
        </div>
         <div class="col-md-3 col-sm-4 col-8 text-right" dir="rtl">
        {% if (window.innerWidth > 480 || !o.options.loadImageFileTypes.test(file.type)) { %}
        <label>عنوان :</label>
        <input type="text" name="file_title[]" class="form-control form-control-sm" value="{%=file.name%}" required>
        {% } %}
        <strong class="error text-danger"></strong>
        </div>
        <div class="col-md-1 d-md-block d-none"></div>
          <div class="col-md-3 col-sm-4 col-8 text-right">
        <div class="size mb-2">Processing...</div>
        <div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar  progress-bar-striped progress-bar-animated bg-success" style="width:0%;float:right !important;"></div></div>
        </div>
        <div class="col-md-3 col-sm-2 col-4 text-left">
{% if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) { %}
        <button class="btn btn-success edit" data-index="{%=i%}" disabled>
        <i class="glyphicon glyphicon-edit"></i>
        <span>Edit</span>
        </button>
        {% } %}
        {% if (!i) { %}
        <button class="btn btn-sm btn-warning cancel">
        <i class="fas fa-ban"></i>
        <span>کنسل</span>
        </button>
        {% } %}
        {% if (!i && !o.options.autoUpload) { %}
        <button class="btn btn-sm btn-primary start" disabled>
        <i class="fas fa-upload"></i>
        <span>آپلود</span>
        </button>
        {% } %}
        </div>
        </div>
        {% } %}
</script>
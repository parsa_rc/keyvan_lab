<header class="elementskit-header main-header">
    <!-- Header Upper -->
    <div class="header-upper">
        <!-- xs-container -->
        <div class="container">
            <div class="xs-navbar clearfix">

                <div class="logo-outer">
                    <div class="logo"><a href="{{ route('home') }}"><img
                                    src="{{ asset('images/logo.png') }}" class="img-fluid"
                                    style="height: 60px " alt=""
                                    title=""></a>
                    </div>
                </div>

                <nav class="elementskit-navbar">
                    <button class=" elementskit-menu-toggler xs-bold-menu">

                        <div class="xs-gradient-group">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <span>
									منو
								</span>
                    </button>
                    <!-- end humberger -->

                    <!-- start menu container -->
                    <div class="elementskit-menu-container elementskit-menu-offcanvas-elements">

                        <!-- start menu item list -->
                        <ul class="elementskit-navbar-nav nav-alignment-dynamic">
                            <li><a href="{{ route('home') }}" class="mx-2">خانه</a></li>
                            @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'home')
                                {{--<li><a href="#featured-section" class="mx-2">ویژگی ها و امکانات</a></li>--}}
                                <li><a href="#rounds-section" class="mx-2">سانس‌ها</a></li>
                            @endif
                            @if(!auth()->check())
                                <li><a href="{{ route('user.login') }}" class="mx-2">جواب‌دهی آنلاین</a></li>
                            @else
                                <li class="elementskit-dropdown-has">
                                    <a href="#">{{ auth()->user()->name }}</a>
                                    <ul class="elementskit-dropdown elementskit-submenu-panel">
                                        <li class="{{ \Illuminate\Support\Facades\Route::currentRouteName()=='user.dashboard' ? 'active' : ''  }}"><a href="{{ route('user.dashboard') }}">پنل کاربری</a></li>
                                        <form id="logout_form" method="post" action="{{ route('user.logout') }}"
                                              class="d-none">
                                            {{ csrf_field() }}
                                        </form>
                                        <li><a onclick="$('#logout_form').submit()" class="mx-2">خروج</a></li>
                                    </ul>
                                </li>
                            @endif
                            <li><a href="{{ route('health.news') }}" class="mx-2">تازه های سلامت</a></li>
                            <li><a href="{{ route('about') }}" class="mx-2">درباره ما</a></li>
                            <li><a href="#contact-us" class="mx-2">تماس با ما</a></li>
                        </ul>

                        <!-- start menu logo and close button (for mobile offcanvas menu) -->
                        <div class="elementskit-nav-identity-panel">
                            <h1 class="elementskit-site-title">
                                <a href="#" class="elementskit-nav-logo">منو</a>
                            </h1>
                            <button class="elementskit-menu-close elementskit-menu-toggler" type="button"><i
                                        class="icon icon-cross"></i></button>
                        </div>
                        <!-- end menu logo and close button -->

                    </div>
                    <!-- end menu container -->

                    <!-- start offcanvas overlay -->
                    <div class="elementskit-menu-overlay elementskit-menu-offcanvas-elements elementskit-menu-toggler">
                    </div>
                    <!-- end offcanvas overlay -->
                    <!-- ---------------------------------------
                            // god menu markup end
                        ---------------------------------------- -->

                </nav>
                {{--<ul class="xs-menu-tools">--}}
                {{--<li>--}}
                {{--<a href="#modal-popup-2" class="navsearch-button xs-modal-popup"><i--}}
                {{--class="icon icon-search"></i></a>--}}
                {{--</li>--}}
                {{--</ul>--}}
            </div>
        </div>
    </div>
    <!-- .container END -->
</header>
@extends('layouts.app')

@section('title','آزمایشگاه کیوان - درباره ما')

@section('content')
    <div class="page-wrapper">
        <div class="preloader"></div>
        @include('layouts.header')
        <hr class="p-0 m-0">

        <div class="col-md-11 mx-auto mt-4 text-right iransans-web">
            <h4 class="iransans-web-bold" dir="rtl">چهار توصیه ی کاربردی در مورد پیشگیری از ابتلا به ویروس کرونا<span style="font-size: 19px">(COVID-19)</span></h4>
            <ul class="mt-4 mb-5" dir="rtl">
                <li class="my-2">
                    <strong>                    شستوشوی دستها:
                    </strong>
                    مرتب دست های خود را بشویید.
                    از یک شوینده الکلی برای شستشوی دست های خود استفاده کنید.
                </li>
                <li class="my-2">
                    <strong>
                        رعایت فاصله با دیگران:
                    </strong>
                    بین شما و کسی که در حال عطسه یا سرفه است باید حداقل ۱ متر
                    فاصله وجود داشته باشد.
                    <br>
                    چرا اینکار مهم است؟ وقتی فردی عطسه یا سرفه می کند، از این راه
                    ذرات مایعی را از بینی یا دهان خود به بیرون پخش می کند؛ ذراتی
                    که ممکن است حاوی ویروس باشند. اگر فاصله درست را با فرد رعایت
                    نکرده باشید، این ذرات از راه تنفس وارد بدن شما می شوند که می
                    توانند ویرس کووید-۱۹ را هم با خود داشته باشند.
                </li>
                <li class="my-2">
                    <strong>عدم لمس چشم ها، بینی و دهان:</strong>
                    همانطور که می دانید دست ها با بسیاری از سطوح تماس داشته و می
                    توانند حاوی ویروس باشند. در صورت آلوده شدن دست های ما می
                    توانند ویروس را به چشم ها، بینی یا دهان ما منتقل کنند. از این
                    اجزای بدن نیز ویروس به داخل بدن راه پیدا کرده و باعث بیماری
                    می شود.
                </li>
                <li class="my-2">
                    <strong>رعایت بهداشت تنفسی:</strong>
                    در هنگام عطسه یا سرفه دهان و بینی خود را با دستمال کاغذی و در
                    صورت عدم دسترسی با آرنج تان بپوشانید. سپس بلافاصله دستمال را
                    در یک سطل زباله دربسته بیندازید.
                </li>
            </ul>
            <div class="my-3">
                <a href="https://film.tebyan.net/film/328028/%D8%A7%D8%AE%D8%AA%D9%84%D8%A7%D9%81-%D8%A7%DB%8C%D8%B1%D8%A7%D9%86-%D9%88-%D8%AC%D9%87%D8%A7%D9%86-%D8%A8%D8%B1%D8%A7%DB%8C-%D9%85%D9%82%D8%A7%D8%A8%D9%84%D9%87-%D8%A8%D8%A7-%DA%A9%D8%B1%D9%88%D9%86%D8%A7-%DA%86%D9%82%D8%AF%D8%B1-%D8%A7%D8%B3%D8%AA#TargetPlayerPos">
                    مقاله اختلاف ایران و جهان برای مقابله با کرونا
                </a>
            </div>
            <div class="my-3">
                <a href="{{ route('pdf.download') }}">
                    دانلود فایل دستورالعمل های بهداشتی کرونا
                </a>
            </div>
        </div>
    </div>
    @include('footer')
@endsection
<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'parent', 'age', 'phone' , 'melli_code' , 'round_id' , 'password' , 'address'
    ];

    protected $hidden=['password'];

    public function rounds(){
        return $this->belongsToMany(Round::class , 'round_users');
    }

    public function attachments(){
        return $this->hasMany(Attachment::class);
    }

    public function attempt(){
        return $this->hasOne(Attempt::class);
    }
}

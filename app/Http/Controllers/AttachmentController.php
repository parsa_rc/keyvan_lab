<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->except(['download']);
    }

    public function store(Request $request,$round_id,$user_id){
        $this->validate($request,[
            'file' => 'required'
        ],[
            'file.required' => 'آپلود کردن نتیجه آزمایش الزامی است'
        ]);

        if($request->hasFile('file')){
            $file = $request->file ;
            $ext = $file->getClientOriginalExtension();
            $path = time().'.'.$ext;
            $file->storeAs(env('FILE_DIR_NAME'),$path);
        }
        Attachment::create([
            'user_id' => $user_id ,
            'round_id' => $round_id ,
            'file' => $path
        ]);

        return redirect()->back();
    }

    public function download($round_id,$user_id){
        if ($a = Attachment::where('round_id',$round_id)->where('user_id',$user_id)->first()){
            $path = env('ORIGINAL_DIR_PATH').env('FILE_DIR_NAME').$a->file;
            return Response::download($path);
        }
    }

    public function update(Request $request ,$round_id,$user_id){
        if ($a = Attachment::where('round_id',$round_id)->where('user_id',$user_id)->first()){

            $this->validate($request,[
                'file' => 'required'
            ],[
                'file.required' => 'آپلود کردن نتیجه آزمایش الزامی است'
            ]);

            if($request->hasFile('file')){
                Storage::delete(env('FILE_DIR_NAME').$a->file);
                $file = $request->file ;
                $ext = $file->getClientOriginalExtension();
                $path = time().'.'.$ext;
                $file->storeAs(env('FILE_DIR_NAME'),$path);
                $a->file = $path;
                $a->save();
            }
        }
        return redirect()->back();
    }

    public function destroy($round_id,$user_id){
        $attachment =  Attachment::where('round_id',$round_id)->where('user_id',$user_id)->first();
        Storage::delete(env('FILE_DIR_NAME').$attachment->file);
        $attachment->delete();
        return redirect()->back();
    }
}

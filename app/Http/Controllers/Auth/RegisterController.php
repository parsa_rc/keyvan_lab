<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Http\Requests\RegisterRequest;
use App\Notifications\UserRegistered;
use App\User;
use Illuminate\Support\Facades\Notification;

class RegisterController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function view()
    {
        return view('auth.register');
    }

    public function store(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password']=bcrypt($data['password']);
        $user=User::create($data);
        auth('web')->login($user);

        Notification::send(Admin::all(),new UserRegistered($user));

        return $user;
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Admin;
use App\Attempt;
use App\Consts;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\lib\zarinpal;
use App\Notifications\UserRegistered;
use App\Price;
use App\Round;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Kavenegar\KavenegarApi;
use Morilog\Jalali\Jalalian;
use nusoap_client;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{

    private $sender = "1000596446";

    public function __construct()
    {
        $this->middleware('auth:admin')->only(['index', 'destroyMultiple', 'all']);
        $this->middleware('auth:web')->only(['dashboard', 'logout', 'pay']);
    }

    public function all()
    {
        $users = User::latest()->get();
//        $rounds = Round::all();
        return view('userall')
            ->with(compact('users'));
//            ->with(compact('rounds'));
    }

    public function otpSend(Request $request)
    {
        $this->validate($request, [
            'phone' => array('required', 'regex:/^0?9[0-39][0-9]{8}$/', $request->has('route') && $request->route == 'user.join' ? 'unique:users,phone' : 'required')
        ], [
            'phone.required' => 'وارد کردن شماره تماس الزامی است',
            'phone.regex' => 'شماره تماس به درستی وارد نشده است',
            'phone.unique' => 'شماره تماس قبلا در سیستم ثبت ثبت شده است'
        ]);
        if ($request->has('route') && $request->route == 'user.login' && User::where('phone', $request->phone)->count() <= 0) {
            return response()->json([
                'phone' => 'شماره مورد نظر شما در سیستم ثبت نشده است'
            ], 404);
        }
        $now = Carbon::now();
        if ($attempter = Attempt::where('phone', $request->phone)->first()) {
            if ($now->diffInDays($attempter->updated_at) >= 1) {
                $attempter->times = 1;
                $attempter->save();
            } else if ($attempter->times >= Attempt::MAX_ATTEMPTS) {
                return response()->json([
                    'phone' => 'تعداد دفعات درخواست کد تایید شما برای ۲۴ ساعت پیش رو به پایان رسیده است'
                ], 429);
            } else {
                $attempter->times = $attempter->times + 1;
                $attempter->save();
            }
        } else {
            Attempt::create([
                'phone' => $request->phone,
                'times' => 1
            ]);
        }
        $receptor = $request->phone;
        session()->put('phone', $receptor);
        $token = random_int(11111, 99999);
        $response = Http::get('https://api.kavenegar.com/v1/'.env('SMS_API').'/verify/lookup.json?receptor='.$receptor.'&token='.$token.'&template=sendOTP');
        $status = $response->json()['return']['status'];
        if ($status == 200){
            session(['otp' => $token]);
            return response()->json(['status' => 200, 'mobile' => $receptor]);
        }
        return response()->json(['status' => 400 , 'mobile' => $receptor]);
    }

    public function otpVerify(Request $request)
    {
        $otp = \request()->session()->get('otp');
        $this->validate($request, [
            'code' => 'required|digits:5'
        ], [
            'code.required' => 'وارد کردن کد تایید اجباری است',
            'code.digits' => 'کد تایید باید ۵ رقمی باشد',
        ]);
        if ($request->code == $otp) {
            \request()->session()->remove('otp');
            \request()->session()->remove('phone');
            return json_encode(['status' => 200]);
        }
        return response()->json(['code' => 'کد تایید وارد شده اشتباه است'], 404);
    }

    public function index($id)
    {
        $round = Round::find($id);
        $users = $round->users;
        return view('round.users')
            ->with(compact('users'))
            ->with(compact('round'));
    }

    public function create(Round $round)
    {
        if ($round->remaining > 0 && Carbon::parse($round->date)->gt(Carbon::now())) {
            return view('auth.register')->with(compact('round'));
        } else {
            return redirect()->back();
        }
    }

    public function prestore(UserRequest $request, $id)
    {
        return "";
    }

    public function store(UserRequest $request, $id)
    {
        $data = $request->all();
        $user = User::create($data);
        $round = Round::find($id);
        if ($round->remaining > 0) {
            $round->remaining = $round->remaining - 1;
            $round->save();
            session([
                'round' => $round,
                'user' => $user,
                'isNew' => true,
                'redirect' => \route('user.join', $round->id)
            ]);
            $order = new zarinpal();
            $res = $order->pay(\route('user.pay.result'), Price::first() ? Price::first()->price : 100000, "moharastegaran@gmail.com", "09057507969");
            return redirect($res);
        } else {
            session()->flash('flash', 'متاسفانه ظرفیت سانس مورد نظر تکمیل شده است, اطلاعات شما در سیستم ذخیره شده است,لطفا سانس های دیگر را امتحان کنید');
            return redirect()->back();
        }
    }

    public function login(Round $round = null)
    {
        if ($round) {
            if ($round->remaining > 0 && Carbon::parse($round->date)->gt(Carbon::now())) {
                return view('auth.login')->with(compact('round'));
            } else {
                throw new NotFoundHttpException();
            }
        }
        return view('auth.login');
    }

    public function check(Request $request, Round $round = null)
    {
        $this->validate($request, [
            'phone' => array('required', 'regex:/^0?9[0-39][0-9]{8}$/')
        ], [
            'phone.required' => 'وارد کردن شماره تماس الزامی است',
            'phone.regex' => 'شماره تماس به درستی وارد نشده است',
        ]);
        if ($user = User::where('phone', $request->phone)->first()) {
            if ($round) {
                if ($round->remaining > 0) {
                    $round->remaining = $round->remaining - 1;
                    $round->save();
                    session([
                        'round' => $round,
                        'user' => $user
                    ]);
                    $order = new zarinpal();
                    $res = $order->pay(\route('user.pay.result'), Price::first() ? Price::first()->price : 100000, "moharastegaran@gmail.com", "09057507969");
                    return redirect($res);
                } else {
                    session()->flash('flash', 'متاسفانه ظرفیت سانس مورد نظر تکمیل شده است, اطلاعات شما در سیستم ذخیره شده است,لطفا سانس های دیگر را امتحان کنید');
                    return redirect()->back();
                }
            } else {
                auth('web')->login($user);
                return redirect()->route('home');
            }
        }
        return redirect()->back()->withErrors([
            'phone' => 'مشکلی در احراز هویت پیش آمد'
        ]);
    }

    public function dashboard()
    {
        $user = auth('web')->user();
        return view('dashboard')->with(compact('user'));
    }

    public function logout()
    {
        auth('web')->logout();
        return redirect()->back();
    }

    public function destroyMultiple(Request $request)
    {
        $ids = $request->deletable;
        Round::destroy($ids);
    }

    public function pay(Round $round)
    {
        if (session()->has('wtf')) {
            return redirect()->home();
        }
        session([
            'user' => auth()->user(),
            'round' => $round
        ]);
        $order = new zarinpal();
        $res = $order->pay(\route('user.pay.result'), Price::first() ? Price::first()->price : 100000, "moharastegaran@gmail.com", "09057507969");
        return redirect($res);
    }

    public function orderResult(Request $request)
    {
        session()->flash('wtf', true);
        if (env('ZARINPAL_IS_MOCKED')) {
            $round = session()->pull('round');
            $user = session()->pull('user');
            $user->rounds()->sync($round->id);
            $date = Jalalian::forge($round->date)->format('13%y/%m/%d');
            $receptor = $user->phone;
            $response = Http::get('https://api.kavenegar.com/v1/'.env('SMS_API').'/verify/lookup.json?receptor='.$receptor.'&token='.$date.'&token2='.$round->start.'&token3='.$round->end.'&template=sendTicket');
            session()->flash('flash', 'نتیجه به صورت پیامک برای شما ارسال خواهد شد');
            $user->isPayed = true;
            $user->save();

            return redirect()->route('home');
        }

        $MerchantID = env('MERCHANT_ID');
        $Authority = $request->get('Authority');

        $Amount = Price::first() ? Price::first()->price : 100000;
        $round = session()->pull('round');
        $user = session()->pull('user');
        if ($request->get('Status') == 'OK') {
            $client = new nusoap_client('https://www.zarinpal.com/pg/services/WebGate/wsdl', 'wsdl');
            $client->soap_defencoding = 'UTF-8';

            //در خط زیر یک درخواست به زرین پال ارسال می کنیم تا از صحت پرداخت کاربر مطمئن شویم
            $result = $client->call('PaymentVerification', [
                [
                    //این مقادیر را به سایت زرین پال برای دریافت تاییدیه نهایی ارسال می کنیم
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ],
            ]);
            if ($result['Status'] == 100) {
                $user->rounds()->attach($round->id);
                if (session()->has('isNew')) {
                    session()->remove('isNew');
                    Notification::send(Admin::all(), new UserRegistered($user));
                }
                $date = Jalalian::forge($round->date)->format('13%y/%m/%d');
                $receptor = $user->phone;
                $response = Http::get('https://api.kavenegar.com/v1/'.env('SMS_API').'/verify/lookup.json?receptor='.$receptor.'&token='.$date.'&token2='.$round->start.'&token3='.$round->end.'&template=sendTicket');
                session()->flash('flash', 'نتیجه به صورت پیامک برای شما ارسال خواهد شد');
                $user->isPayed = true;
                $user->save();

                return redirect()->route('home');
            } else {
                $round->remaining = $round->remaining + 1;
                $round->save();
//                session()->flash('flash','متاسفانه ظرفیت سانس مورد نظر تکمیل شده است, اطلاعات شما در سیستم ذخیره شده است,لطفا سانس های دیگر را امتحان کنید');
                return redirect()->back();
            }
        } else {
            $round->remaining = $round->remaining + 1;
            $round->save();
//            session()->flash('flash','متاسفانه ظرفیت سانس مورد نظر تکمیل شده است, اطلاعات شما در سیستم ذخیره شده است,لطفا سانس های دیگر را امتحان کنید');
            return redirect()->back();
        }
    }
}

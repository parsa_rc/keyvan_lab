<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except(['showLoginForm', 'check']);
    }

    public function showLoginForm()
    {
        return view('auth.admin.login');
    }

    public function check(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth('admin')->attempt($credentials)) {
            return redirect()->route('admin.home');
        }

        return redirect()->back()->withErrors([
            'email' => 'اطلاعات وارد شده اشتباه است'
        ]);

    }

    public function template()
    {
        return view('layouts.admin.home');
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->to('/');
    }

    public function markAsRead()
    {
        auth('admin')->user()->newMembers()->markAsRead();
    }

}

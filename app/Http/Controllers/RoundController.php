<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoundRequest;
use App\Round;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\CalendarUtils;

class RoundController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rounds = Round::select('date', DB::raw('count(*) as total'))->groupBy('date')->orderBy('date', 'DESC')->get();
        return view('round.index')->with(compact('rounds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('round.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RoundRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoundRequest $request)
    {
        $data = $request->all();
        $data['remaining'] = $request->capacity;
        $data['date'] = CalendarUtils::createCarbonFromFormat('Y-m-d', $data['date'])->toDateString();
        Round::create($data);
        return redirect()->route('round.show', $data['date']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rounds = Round::where('date', $id)->get();
        return view('round.indexpost')->with(compact('rounds'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $round = Round::find($id);
        return view('round.edit')
            ->with(compact('round'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoundRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoundRequest $request, $id)
    {
        $data = $request->all();
        $r = Round::find($id);
        if ($data['capacity'] < $r->remaining ){
            $data['remaining'] = $data['capacity'];
        } else if ($data['capacity'] > $r->capacity) {
            $data['remaining'] = ($data['capacity'] - $r->capacity) + $r->remaining;
        }
        $data['date'] = CalendarUtils::createCarbonFromFormat('Y-m-d', $data['date'])->toDateString();
        $r->update($data);
        return redirect()->route('round.show', $r->date);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $round = Round::find($id);
        $round->delete();
        return redirect()->back();
    }

    public function destroyMultiple(Request $request)
    {
        $dates = $request->deletable;
        foreach ($dates as $date)
            Round::where('date', $date)->first()->delete();
    }
}

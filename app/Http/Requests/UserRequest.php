<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string' ,
            'parent' => 'required|string' ,
            'age' => 'required|numeric' ,
            'melli_code' => 'required|digits:10|unique:users,melli_code',
            'phone' => 'required|regex:/^0?9[0-39][0-9]{8}$/|unique:users,phone',
            'address' => 'required'
//            'password' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'وارد کردن نام کامل الزامی است' ,
            'parent.required' => 'وارد کردن نام پدر الزامی است' ,
            'age.required' => 'وارد کردن سن الزامی است' ,
            'age.numeric' => 'سن را به صورت عددی وارد کنید' ,
            'melli_code.required' => 'وارد کردن کد ملی الزامی است',
            'melli_code.digits' => 'کد ملی را ۱۰ رقمی وارد کنید' ,
            'melli_code.unique' => 'کد ملی قبلا در سیستم ثبت شده است' ,
            'phone.required' => 'وارد کردن شماره تماس الزامی است' ,
            'phone.regex' => 'شماره تماس به درستی وارد نشده است' ,
            'phone.unique' => 'شماره تماس قبلا در سیستم ثبت ثبت شده است',
            'address.required' => 'وارد کردن آدرس الزامی است'
//            'password.required' => 'وارد کردن گذرواژه الزامی است' ,
//            'password.min' => 'گذرواژه حداقل ۵ رقمی باشد'
        ];
    }
}

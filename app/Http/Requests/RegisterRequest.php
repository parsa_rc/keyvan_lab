<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5',
            'name' => 'required',
            'family' => 'required',
            'mobile' => 'required|regex:/^0?9[0-39][0-9]{8}$/|unique:users,mobile',
//            'age' => 'required|numeric',
//            'isMale' => 'required',
//            'blood_type' => 'required',
//            'img' => 'required|mimes:jpg,png,jpeg,tiff',

//            'height' => 'required|numeric',
//            'weight' => 'required|numeric',
//            'neck' => 'required|numeric',
//            'arm' => 'required|numeric',
//            'contracted_arm' => 'required|numeric',
//            'forearm' => 'required|numeric',
//            'wrist' => 'required|numeric',
//            'chest' => 'required|numeric',
//            'waist' => 'required|numeric',
//            'hip' => 'required|numeric',
//            'thigh' => 'required|numeric',
//            'leg' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'وارد گردن ایمیل الزامی است, زیرا تنها راه ارسال برنامه برای شماست',
            'email.email' => 'فرمت ایمیل را به درستی وارد کنید',
            'email.unique' => 'ایمیل قبلا در سیستم ثبت شده است',

            'password.required' => 'وارد کردن گزرواژه الزامی است',
            'password.min' => 'گذرواژه حداقل ۴ رقمی است',

            'name.required' => 'وارد کردن نام الزامی است',
            'family.required' => 'وارد کردن نام خانوادگی الزامی است',

//            'age.required' => 'وارد کردن سن الزامی است',
//            'age.numeric' => 'سن باید به صورت عدد وارد شود',

            'mobile.required' => 'وارد کردن شماره تماس الزامی است',
            'mobile.regex' => 'شماره تماس به اشتباه وارد شده است',
            'mobile.unique' => 'شماره تماس قبلا ثبت شده است' ,

//            'isMale.required' => 'وارد کردن جنسیت الزامی است',
//            'blood_type.required' => 'وارد کردن گروه خونی الزامی است',
//
//            'img.required' => 'وارد کردن عکس الزامی است',
//            'img.mimes' => 'فرمت عکس اشتباه است'
        ];
    }
}

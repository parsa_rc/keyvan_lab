<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attempt extends Model
{
    const MAX_ATTEMPTS = 5;

    protected $fillable=['phone','times'];
}

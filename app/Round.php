<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    protected $fillable = [
       'date','start' , 'end' , 'capacity' , 'remaining'
    ];

    public function users(){
        return $this->belongsToMany(User::class , 'round_users');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable= ['round_id','user_id' , 'file'];

    public function round(){
        return $this->belongsTo(Round::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}

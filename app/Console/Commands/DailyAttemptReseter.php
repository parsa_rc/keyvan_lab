<?php

namespace App\Console\Commands;

use App\Attempt;
use Illuminate\Console\Command;

class DailyAttemptReseter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'day:attemptReset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this attribute resets user requests for requesting otp every day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $attempts = Attempt::all();
        foreach ($attempts as $attempt){
            $attempt->delete();
        }
    }
}

<?php

use Illuminate\Support\Facades\Route;
use Morilog\Jalali\Jalalian;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');

Route::view('/about','about')->name('about');
Route::view('/health-news','health')->name('health.news');

Route::get('/download/voice',function (){
    return response()->download(base_path("about_corona_virus.mpeg"));
})->name('voice.download');

Route::get('/pdf/download',function (){
    return \Illuminate\Support\Facades\Response::download(base_path('corona_recommendation.pdf'));
})->name('pdf.download');

Route::prefix('admin')->name('admin.')->namespace('Auth')->group(function () {
    Route::get('home', 'AdminController@template')->name('home');
    Route::get('login', 'AdminController@showLoginForm')->name('login.view');
    Route::post('login/check', 'AdminController@check')->name('login.check');
    Route::post('logout', 'AdminController@logout')->name('logout');
    Route::get('users/notifications/markAsRead', 'AdminController@markAsRead')->name('users.notifications.markAsRead');
});

Route::prefix('user')->name('user.')->namespace('Auth')->group(function () {
    Route::get('round/{id}', 'UserController@index')->name('round');
    Route::get('join/{round}', 'UserController@create')->name('join');
    Route::get('login/{round?}', 'UserController@login')->name('login');
    Route::post('login/check/{round?}', 'UserController@check')->name('check');
    Route::get('panel', 'UserController@dashboard')->name('dashboard');
    Route::post('logout', 'UserController@logout')->name('logout');
    Route::post('pre/store/{id}', 'UserController@prestore')->name('prestore');
    Route::post('store/{id}', 'UserController@store')->name('store');
    Route::get('pay/start/{round}', 'UserController@pay')->name('pay.start');
    Route::get('pay/result', 'UserController@orderResult')->name('pay.result');
    Route::post('otp/send', 'UserController@otpSend')->name('otp.send');
    Route::post('otp/verify', 'UserController@otpVerify')->name('otp.verify');
    Route::get('all', 'UserController@all')->name('all');
});

Route::prefix('attachment')->name('attachment.')->group(function () {
    Route::post('store/{round_id}/{user_id}', 'AttachmentController@store')->name('store');
    Route::get('download/{round_id}/{user_id}', 'AttachmentController@download')->name('download');
    Route::put('update/{round_id}/{user_id}', 'AttachmentController@update')->name('update');
    Route::delete('destroy/{round_id}/{user_id}', 'AttachmentController@destroy')->name('destroy');
});

Route::delete('round/destroy/multiple', 'RoundController@destroyMultiple')->name('round.destroy.multiple');
Route::delete('round/users/destroy/multiple', 'RoundController@destroyMultiple')->name('round.users.destroy.multiple');
Route::resource('round', 'RoundController');

Route::get('price/set', function () {
    if (\App\Price::count() > 0) {
        $price = \App\Price::first()->price;
    } else {
        $price = null;
    }
    return view('price')->with([
        'price' => $price
    ]);
})->name('price.set')->middleware('auth:admin');
Route::post('price/change', function (\Illuminate\Http\Request $request) {
    \Illuminate\Support\Facades\Validator::make([$request], [
        'price' => 'required|numeric'
    ], [
        'price.required' => 'وارد کردن قیمت الزامی است',
        'price.numeric' => 'قیمت باید به صورت عددی باشد'
    ]);
    if (\App\Price::count() > 0) {
        $price = \App\Price::first();
        $price->price = $request->price;
        $price->save();
    } else {
        \App\Price::create([
            'price' => $request->price
        ]);
    }
    return redirect()->route('admin.home');
})->name('price.change')->middleware('auth:admin');